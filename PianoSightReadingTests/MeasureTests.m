//
//  MeasureTests.m
//  PianoSightReading
//
//  Created by Tiago Bras on 29/11/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import "MeasureTests.h"
#import "NotationSymbols.h"
#import "Measure.h"
#import "Element.h"
#import "Voice.h"
#import "Note.h"

#define NewNote(varTick) ([[Note alloc] initWithPitch:MidiMiddleC tick:varTick])
#define NewVoice(varElements) ([[Voice alloc] initWithElements:varElements])
#define NewMeasure4_4(varVoices) ([[Measure alloc] initWithVoices:varVoices clef:TrebleClef key:KeyMajorC timeSignature:TimeSignature4_4])

@implementation MeasureTests

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testValidadeVoices {
    Note *n1 = [[Note alloc] initWithPitch:MidiMiddleC tick:4];
    Note *n2 = [[Note alloc] initWithPitch:MidiMiddleC tick:8];
    Note *n3 = [[Note alloc] initWithPitch:MidiMiddleC tick:4];
    
    Note *n4 = [[Note alloc] initWithPitch:MidiMiddleC tick:2];
    Note *n5 = [[Note alloc] initWithPitch:MidiMiddleC tick:2];
    Note *n6 = [[Note alloc] initWithPitch:MidiMiddleC tick:10];
    Note *n7 = [[Note alloc] initWithPitch:MidiMiddleC tick:2];
    
    Note *n8 = [[Note alloc] initWithPitch:MidiMiddleC tick:4];
    Note *n9 = [[Note alloc] initWithPitch:MidiMiddleC tick:4];
    
    Voice *v1 = [[Voice alloc] initWithElements:@[n1, n2, n3]];
    Voice *v2 = [[Voice alloc] initWithElements:@[n4, n5, n6, n7]];
    Voice *v3 = [[Voice alloc] initWithElements:@[n8, n9]];
    
    Measure *measure = [[Measure alloc] initWithVoices:@[v1]
                                                  clef:TrebleClef
                                          key:KeyMajorC
                                         timeSignature:TimeSignature4_4];
    
    STAssertTrue([measure countVoices] == 1, @"Wrong num of voices.");
    
    [measure addVoice:v2];
    
    STAssertTrue([measure countVoices] == 2, @"Wrong num of voices.");
    
    STAssertTrue([measure validateVoices], @"Voices are invalid.");
    
    // Add voice with different total ticks than v1 and v2
    [measure addVoice:v3];
    
    STAssertFalse([measure validateVoices], @"Voices are invalid.");
}

- (void)testMinWidth {
    Note *n1 = [[Note alloc] initWithPitch:MidiMiddleC tick:4];
    Note *n2 = [[Note alloc] initWithPitch:MidiMiddleC tick:8];
    Note *n3 = [[Note alloc] initWithPitch:MidiMiddleC tick:4];
    
    Note *n4 = [[Note alloc] initWithPitch:MidiMiddleC tick:2];
    Note *n5 = [[Note alloc] initWithPitch:MidiMiddleC tick:2];
    Note *n6 = [[Note alloc] initWithPitch:MidiMiddleC tick:10];
    Note *n7 = [[Note alloc] initWithPitch:MidiMiddleC tick:2];
    
    [n1 setMinWidth:4.0];
    [n2 setMinWidth:2.0];
    [n3 setMinWidth:1.5];   // Total min width = 7.5
    
    [n4 setMinWidth:1.0];
    [n5 setMinWidth:1.5];
    [n6 setMinWidth:2.0];
    [n7 setMinWidth:1.5];   // Total min width = 6.0

    Voice *v1 = [[Voice alloc] initWithElements:@[n1, n2, n3]];
    Voice *v2 = [[Voice alloc] initWithElements:@[n4, n5, n6, n7]];
    
    Measure *measure = [[Measure alloc] initWithVoices:@[v2]
                                                  clef:TrebleClef
                                          key:KeyMajorC
                                         timeSignature:TimeSignature4_4];
    
    STAssertTrue([measure totalMinWidth] == 6.0, @"Wrong total min width.");
    
    [measure addVoice:v1];
    
    STAssertTrue([measure totalMinWidth] == 7.5, @"Wrong total min width.");
}

- (void)testAlignedTickables {    
    Note *n1 = NewNote(3);
    Note *n2 = NewNote(6);
    Note *n3 = NewNote(3);
    
    Note *n4 = NewNote(2);
    Note *n5 = NewNote(2);
    Note *n6 = NewNote(2);
    Note *n7 = NewNote(6);
    
    Note *n8 = NewNote(4);
    Note *n9 = NewNote(2);
    Note *n10 = NewNote(3);
    Note *n11 = NewNote(3);
    
    Note *n12 = NewNote(9);
    Note *n13 = NewNote(3);
    
    Voice *v1 = NewVoice((@[n1, n2, n3]));
    Voice *v2 = NewVoice((@[n4, n5, n6, n7]));
    Voice *v3 = NewVoice((@[n8, n9, n10, n11]));
    Voice *v4 = NewVoice((@[n12, n13]));
    
    Measure *measure = NewMeasure4_4((@[v1, v2, v3, v4]));
    
    NSArray *a1 = [measure alignedTickablesIndexes:v1 v2:v2];
    
    STAssertTrue([a1 count] == 2, @"Wrong number of aligned tickables");
    
    NSArray *a2 = [measure alignedTickablesIndexes:v1 v2:v3];
    
    STAssertTrue([a2 count] == 3, @"Wrong number of aligned tickables");
    
    NSArray *a3 = [measure alignedTickablesIndexes:v2 v2:v3];
    
    STAssertTrue([a3 count] == 4, @"Wrong number of aligned tickables");
}

- (void)testLayout {
    Note *n1 = NewNote(3);
    Note *n2 = NewNote(6);
    Note *n3 = NewNote(3);
    
    Note *n4 = NewNote(2);
    Note *n5 = NewNote(2);
    Note *n6 = NewNote(2);
    Note *n7 = NewNote(6);
    
    Note *n8 = NewNote(4);
    Note *n9 = NewNote(2);
    Note *n10 = NewNote(3);
    Note *n11 = NewNote(3);
    
    Note *n12 = NewNote(9);
    Note *n13 = NewNote(3);
    
    Voice *v1 = NewVoice((@[n1, n2, n3]));
    Voice *v2 = NewVoice((@[n4, n5, n6, n7]));
    Voice *v3 = NewVoice((@[n8, n9, n10, n11]));
    Voice *v4 = NewVoice((@[n12, n13]));
    
    Measure *measure = NewMeasure4_4((@[v1, v2, v3, v4]));
    
//    [measure layout:20.0 xOffset:0.0];
}

@end
