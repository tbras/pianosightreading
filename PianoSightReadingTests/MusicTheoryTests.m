//
//  MusicTheoryTests.m
//  PianoSightReading
//
//  Created by Tiago Bras on 18/12/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import "MusicTheoryTests.h"
#import "TheoryHelper.h"

@implementation MusicTheoryTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testSignatureForKey {
    int map[KeysCount];
    
    map[KeyMajorCb] = -7;
    map[KeyMajorC] = 0;
    map[KeyMajorCSharp] = 7;
    map[KeyMajorDb] = -5;
    map[KeyMajorD] = 2;
    map[KeyMajorEb] = -3;
    map[KeyMajorE] = 4;
    map[KeyMajorF] = -1;
    map[KeyMajorFSharp] = 6;
    map[KeyMajorGb] = -6;
    map[KeyMajorG] = 1;
    map[KeyMajorAb] = -4;
    map[KeyMajorA] = 3;
    map[KeyMajorBb] = -2;
    map[KeyMajorB] = 5;
    
    map[KeyMinorC] = -3;
    map[KeyMinorCSharp] = 4;
    map[KeyMinorD] = -1;
    map[KeyMinorDSharp] = 6;
    map[KeyMinorEb] = -6;
    map[KeyMinorE] = 1;
    map[KeyMinorF] = -4;
    map[KeyMinorFSharp] = 3;
    map[KeyMinorG] = -2;
    map[KeyMinorGSharp] = 5;
    map[KeyMinorAb] = -7;
    map[KeyMinorA] = 0;
    map[KeyMinorASharp] = 7;
    map[KeyMinorBb] = -5;
    map[KeyMinorB] = 2;
    
    for (int i = 0; i < KeysCount; i++) {
        STAssertEquals(map[i], [TheoryHelper signatureForKey:i], @"Signature for key %d is wrong.", i);
    }
}

- (void)testSemitoneDegrees {
    STAssertEquals([TheoryHelper semitoneInCMajor:KeyMajorCb], -1, @"Wrong semitone degree.");
    STAssertEquals([TheoryHelper semitoneInCMajor:KeyMajorA], 9, @"Wrong semitone degree.");
    STAssertEquals([TheoryHelper semitoneInCMajor:KeyMajorAb], 8, @"Wrong semitone degree.");
    STAssertEquals([TheoryHelper semitoneInCMajor:KeyMajorC], 0, @"Wrong semitone degree.");
    STAssertEquals([TheoryHelper semitoneInCMajor:KeyMinorB], 11, @"Wrong semitone degree.");
    
    int s1 = [TheoryHelper semitoneDegreeForPitch:MidiMiddleC key:KeyMinorB];
    int s2 = [TheoryHelper semitoneDegreeForPitch:MidiMiddleC key:KeyMajorCb];
    int s3 = [TheoryHelper semitoneDegreeForPitch:MidiPitchB3 key:KeyMinorC];
    int s4 = [TheoryHelper semitoneDegreeForPitch:MidiPitchA0 key:KeyMinorDSharp];
    int s5 = [TheoryHelper semitoneDegreeForPitch:MidiPitchG1 key:KeyMajorG];
    int s6 = [TheoryHelper semitoneDegreeForPitch:MidiPitchB6 key:KeyMajorCb];
    
    STAssertEquals(s1, 1, @"Wrong semitone for degree and key.");
    STAssertEquals(s2, 1, @"Wrong semitone for degree and key.");
    STAssertEquals(s3, 11, @"Wrong semitone for degree and key.");
    STAssertEquals(s4, 6, @"Wrong semitone for degree and key.");
    STAssertEquals(s5, 0, @"Wrong semitone for degree and key.");
    STAssertEquals(s6, 0, @"Wrong semitone for degree and key.");
    
    STAssertEquals([TheoryHelper semitoneDegreeForScaleDegree:SD_I], 0,
                   @"Wrong semitone for scale degree.");
    STAssertEquals([TheoryHelper semitoneDegreeForScaleDegree:SD_II], 2,
                   @"Wrong semitone for scale degree.");
    STAssertEquals([TheoryHelper semitoneDegreeForScaleDegree:SD_IVb], 4,
                   @"Wrong semitone for scale degree.");
    STAssertEquals([TheoryHelper semitoneDegreeForScaleDegree:SD_VII], 11,
                   @"Wrong semitone for scale degree.");
    
    STAssertEquals([TheoryHelper octaveForPitch:MidiMiddleC key:KeyMajorCb], 4,
                   @"Wrong octave for pitch and octave.");
    STAssertEquals([TheoryHelper octaveForPitch:MidiPitchC3 key:KeyMinorC], 3,
                   @"Wrong octave for pitch and octave.");
    STAssertEquals([TheoryHelper octaveForPitch:MidiPitchA0 key:KeyMajorC], 0,
                   @"Wrong octave for pitch and octave.");
    STAssertEquals([TheoryHelper octaveForPitch:MidiPitchE3 key:KeyMajorB], 3,
                   @"Wrong octave for pitch and octave.");
    
    int line1 = [TheoryHelper lineForPitch:MidiMiddleC key:KeyMajorC clef:TrebleClef];
    int line2 = [TheoryHelper lineForPitch:MidiMiddleC key:KeyMajorC clef:BassClef];
    
    int line3 = [TheoryHelper lineForPitch:MidiPitchF5 key:KeyMajorE clef:TrebleClef];
    int line4 = [TheoryHelper lineForPitch:MidiPitchF5 key:KeyMajorE clef:BassClef];
    
    int line5 = [TheoryHelper lineForPitch:MidiPitchB3 key:KeyMajorCb clef:TrebleClef];
    int line6 = [TheoryHelper lineForPitch:MidiPitchB3 key:KeyMajorCb clef:BassClef];
    
    int line7 = [TheoryHelper lineForPitch:MidiPitchE3 key:KeyMajorB clef:TrebleClef];
    int line8 = [TheoryHelper lineForPitch:MidiPitchE3 key:KeyMajorB clef:BassClef];
    
    
    STAssertEquals(line1, -6, @"Wrong line for pitch, key and clef.");
    STAssertEquals(line2, 6, @"Wrong line for pitch, key and clef.");
    STAssertEquals(line3, 4, @"Wrong line for pitch, key and clef.");
    STAssertEquals(line4, 16, @"Wrong line for pitch, key and clef.");
    STAssertEquals(line5, -6, @"Wrong line for pitch, key and clef.");
    STAssertEquals(line6, 6, @"Wrong line for pitch, key and clef.");
    STAssertEquals(line7, -11, @"Wrong line for pitch, key and clef.");
    STAssertEquals(line8, 1, @"Wrong line for pitch, key and clef.");
}

#define TrebleLineForPitch(pitch, k) [TheoryHelper lineForPitch:pitch key:k clef:TrebleClef]
#define BassLineForPitch(pitch, key) [TheoryHelper lineForPitch:pitch key:k clef:BassClef]
- (void)testLineForPitch {
    NSArray *values = @[
    @(TrebleLineForPitch(MidiPitchF3, KeyMajorB)),
    @(TrebleLineForPitch(MidiPitchF3+1, KeyMajorB)),
    @(TrebleLineForPitch(MidiPitchF3+2, KeyMajorB)),
    @(TrebleLineForPitch(MidiPitchF3+3, KeyMajorB)),
    @(TrebleLineForPitch(MidiPitchF3+4, KeyMajorB)),
    @(TrebleLineForPitch(MidiPitchF3+5, KeyMajorB)),
    @(TrebleLineForPitch(MidiPitchF3+6, KeyMajorB)),
    @(TrebleLineForPitch(MidiPitchF3+7, KeyMajorB)),
    @(TrebleLineForPitch(MidiPitchF3+8, KeyMajorB)),
    @(TrebleLineForPitch(MidiPitchF3+9, KeyMajorB)),
    @(TrebleLineForPitch(MidiPitchF3+10, KeyMajorB)),
    @(TrebleLineForPitch(MidiPitchF3+11, KeyMajorB)),
    @(TrebleLineForPitch(MidiPitchF3+12, KeyMajorB))
    ];
    
//    NSArray *valuesExpected = @[
//    @(-10), @(-)
//    ];
}

@end
