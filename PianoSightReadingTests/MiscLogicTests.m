//
//  MiscLogicTests.m
//  PianoSightReading
//
//  Created by Tiago Bras on 28/11/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import "MiscLogicTests.h"
#import "TBUtils.h"

@implementation MiscLogicTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testLiterals
{
    int intvar = 10;
    NSString *svar = @"Hello World";
    NSDictionary *d = @{@(intvar): svar};
    
    STAssertEquals(d[@(intvar)], svar, @"Dictionary literal failed.");
}

- (void)testPermutation {
    int size1 = 4;
    int size2 = 2;
    int size3 = 1;
    NSMutableArray *m1 = [NSMutableArray array];
    NSMutableArray *m2 = [NSMutableArray array];
    NSMutableArray *m3 = [NSMutableArray array];
    
    // Expected: (0, 1), (0, 2), (0, 3), (1, 2), (1, 3), (2, 3)
    for (int i = 0; i < size1 - 1; i++) {
        for (int j = i + 1; j < size1; j++) {
            [m1 addObject:@[@(i), @(j)]];
        }
    }
    
    // Expected: (0, 1)
    for (int i = 0; i < size2 - 1; i++) {
        for (int j = i + 1; j < size2; j++) {
            [m2 addObject:@[@(i), @(j)]];
        }
    }
    
    // Expected: nil
    for (int i = 0; i < size3 - 1; i++) {
        for (int j = i + 1; j < size3; j++) {
            [m3 addObject:@[@(i), @(j)]];
        }
    }
    
    NSArray *expected1 = [NSArray arrayWithObjects:@[@0, @1],
                         @[@0, @2], @[@0, @3], @[@1, @2], @[@1, @3], @[@2, @3], nil];
    NSArray *expected2 = [NSArray arrayWithObjects:@[@0, @1], nil];
    NSArray *expected3 = [NSArray arrayWithObjects:nil];
    
    STAssertTrue([m1 isEqualToArray:expected1], @"Permutation array is wrong.");
    STAssertTrue([m2 isEqualToArray:expected2], @"Permutation array is wrong.");
    STAssertTrue([m3 isEqualToArray:expected3], @"Permutation array is wrong.");
    STAssertFalse([m1 isEqualToArray:expected2], @"Permutation array is wrong.");
}

- (void)testNumberArrayToCArray {
    NSArray *numbers = @[@20, @10, @5, @15];
    
    int *cArray = [TBUtils numberArrayToCArray:numbers];
    
    STAssertEquals([[numbers objectAtIndex:2] intValue], cArray[2],
                   @"Numbers array to c array failed.");
    
    free(cArray);
    cArray = NULL;
    
    cArray = [TBUtils numberArrayToCArray:@[]];
    
    STAssertTrue(cArray == NULL, @"Numbers array to c array failed.");
}

- (void)testCPointers {
    const int ppsize = 5;
    int psizes[ppsize] = {3, 1, 4, 5, 7};
    int **pp = malloc(sizeof(int *) * ppsize);
    
    for (int i = 0; i < ppsize; i++) {
        pp[i] = malloc(sizeof(int) * psizes[i]);
        
        for (int j = 0; j < psizes[i]; j++) {
            pp[i][j] = (int)arc4random() % 10;
        }
    }
    
    for (int i = 0; i < ppsize; i++) {
        free(pp[i]);
    }
    
    free(pp);
    
    IntegerCArray a = IntegerCArrayAlloc(20);
    
    STAssertTrue(a.size == 20, @"Malloc must have failed.");
    
    IntegerCArrayFree(a);
}


@end
