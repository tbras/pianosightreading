//
//  PianoSightReadingTests.m
//  PianoSightReadingTests
//
//  Created by Tiago Bras on 18/10/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import "PianoSightReadingTests.h"
#import "NotationSymbols.h"
#import "Element.h"
#import "Voice.h"

@implementation PianoSightReadingTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testSymbolsCode
{
    NSString *s = [[NotationSymbols sharedInstance] stringForSymbol:Sym_z inFont:FontGonville];
    NSString *expected = @"z";
    
    STAssertEquals(s, expected, @"Symbol string doesn't match!");
}

- (void)testVoice {
    // Array with 7 elements
    NSArray *elements = @[[[Element alloc] init],
        [[Element alloc] init],
        [[Element alloc] init],
        [[Element alloc] init],
        [[Element alloc] init],
        [[Element alloc] init],
        [[Element alloc] init]];
    
    int ticks[7] = {0, 2, 8, 10, 12, 20, 30};
    float minWidths[7] = {2.0, 4.0, 1.5, 1.5, 2.0, 3.0, 5.5};
    
    // Set ticks
    for (int i = 0; i < [elements count]; i++) {
        [elements[i] setTick:ticks[i]];
        [elements[i] setMinWidth:minWidths[i]];
    }
    
    Voice *v = [[Voice alloc] initWithElements:elements];
    
    STAssertTrue([v count] == [elements count], @"Voice count %d != %d",
                 [v count], [elements count]);
    
    // Test total min width
    STAssertTrue([v totalMinWidth] == 19.5, @"Wrong voice total min width.");
    
    // Test accumulated array
    NSArray *accTickArray = [v accTickArray];
    NSArray *accTickArrayExpected = @[@0, @2, @10, @20, @32, @52, @82];
    
    STAssertTrue([accTickArray count] == [accTickArrayExpected count],
                 @"Accumulated tick array should have size 8");
    
    for (int i = 0; i < [accTickArray count]; i++) {
        STAssertEquals([(NSNumber *)accTickArray[i] intValue],
                       [(NSNumber *)accTickArrayExpected[i] intValue], @"Accumulated tick array is not what was expected.");
    }
    
}

- (void)testMeasure {
    // TODO: implement tests once classes Note and Chords are implemented
}

@end
