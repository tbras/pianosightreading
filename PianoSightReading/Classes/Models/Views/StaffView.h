//
//  StaffView.h
//  PianoSightReading
//
//  Created by Tiago Bras on 25/10/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import "ScalableView.h"
#import "Staff.h"
#import "MusicNotationConfig.h"

@interface StaffView : ScalableView

@property(nonatomic, strong, readonly) Staff *staff;
@property(nonatomic, strong) MusicNotationConfig *config;
@property(nonatomic, strong) NSMutableArray *points;

- (id)initWithConfig:(MusicNotationConfig *)config frame: (CGRect)frame;

@end
