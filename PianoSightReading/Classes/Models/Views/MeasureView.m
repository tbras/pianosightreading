//
//  MeasureView.m
//  PianoSightReading
//
//  Created by Tiago Bras on 25/10/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import "MeasureView.h"

@implementation MeasureView

@synthesize measure = _measure;
@synthesize number = _number;
@synthesize config = _config;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (id)initWithMeasure:(Measure *)measure
               number:(int)number
               config:(MusicNotationConfig *)config {
    
}


- (void)draw:(CGContextRef)ctx {
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
