//
//  ScalableView.m
//  PianoSightReading
//
//  Created by Tiago Bras on 25/10/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import "ScalableView.h"

@implementation ScalableView

@synthesize originalFrame = _originalFrame;
@synthesize scale = _scale;

- (id)initWithFrame:(CGRect)frame scale:(float)scale
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        [self setOpaque:YES];
        _originalFrame = frame;
        _scale = scale;
    }
    return self;
}

- (void)draw:(CGContextRef)ctx {
    
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextSaveGState(ctx);
    
    CGContextConcatCTM(ctx, CGAffineTransformMakeScale(_scale, _scale));
    
    [self draw:ctx];
    
    CGContextRestoreGState(ctx);
}


// Override Scale to resize and reposition all subviews
- (void)setScale:(float)scale {
    _scale = scale;
    
    printFrame(_originalFrame);
    CGRect newFrame = CGRectMake(_originalFrame.origin.x * _scale,
                                 _originalFrame.origin.y * _scale,
                                 _originalFrame.size.width * _scale,
                                 _originalFrame.size.height * _scale);
    printFrame(newFrame);
    [self setFrame:newFrame];
    
    [self setNeedsDisplay];
    
    for (UIView *view in [self subviews]) {
        if ([view isKindOfClass:[ScalableView class]]) {
            [(ScalableView *)view setScale:scale];
            [(ScalableView *)view setNeedsDisplay];
        }
    }
}


@end
