//
//  RestView.h
//  PianoSightReading
//
//  Created by Tiago Bras on 25/10/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import "ScalableView.h"
#import "Rest.h"

@interface RestView : ScalableView

@property(nonatomic, strong, readonly) Rest *rest;

@end
