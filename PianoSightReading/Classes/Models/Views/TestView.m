//
//  TestView.m
//  PianoSightReading
//
//  Created by Tiago Bras on 26/10/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import "TestView.h"
#import "StaffView.h"
#import "Note.h"
#import "Staff.h"
#import "Measure.h"
#import "Voice.h"

@implementation TestView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
//        MusicNotationConfig *config = [[MusicNotationConfig alloc] init];
//        CGRect sRect = CGRectMake(0.0, 0.0, super.frame.size.width - 20.0, 200.0);
//        StaffView *s = [[StaffView alloc] initWithConfig:config frame:sRect];
//        
//        [self addSubview:s];
        [self setBackgroundColor:[UIColor whiteColor]];
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
#define Note(PITCH, TICK) [[Note alloc] initWithPitch:PITCH tick:TICK]
    
    
    Note *n1 = Note(MidiMiddleC, 6);
    [n1 setAccidental:AccidentalFlat];
    Note *n2 = Note(MidiPitchD4, 6);
    [n2 setAccidental:AccidentalNatural];
    Note *n3 = Note(MidiPitchC4, 12);
    [n3 setAccidental:AccidentalSharp];
    //    Note *n4 = [[Note alloc] initWithPitch:MidiMiddleC tick:3];
    //    Note *n5 = [[Note alloc] initWithPitch:MidiMiddleC tick:3];
    //
    //    Note *n6 = [[Note alloc] initWithPitch:MidiMiddleC tick:3];
    //    Note *n7 = [[Note alloc] initWithPitch:MidiMiddleC tick:12];
    //    Note *n8 = [[Note alloc] initWithPitch:MidiMiddleC tick:9];
    //
    //    Note *n9 = [[Note alloc] initWithPitch:MidiMiddleC tick:2];
    //    Note *n10 = [[Note alloc] initWithPitch:MidiMiddleC tick:2];
    //    Note *n11 = [[Note alloc] initWithPitch:MidiMiddleC tick:2];
    //    Note *n12 = [[Note alloc] initWithPitch:MidiMiddleC tick:6];
    //    Note *n13 = [[Note alloc] initWithPitch:MidiMiddleC tick:12];
    
    Voice *v1 = [[Voice alloc] initWithElements:@[
                 Note(MidiMiddleC+7, 3),
                 Note(MidiMiddleC-7, 9),
                 Note(MidiMiddleC+19, 3),
                 Note(MidiMiddleC+13, 6),
                 Note(MidiMiddleC+12, 3),
                 Note(MidiMiddleC+3, 3),
                 Note(MidiMiddleC+5, 9),
                 Note(MidiMiddleC+1, 3),
                 Note(MidiMiddleC+5, 3),
                 Note(MidiMiddleC+6, 3),
                 Note(MidiMiddleC+1, 3)]];
    
    Voice *v2 = [[Voice alloc] initWithElements:@[
                 Note(MidiPitchC5+1, 8),
                 Note(MidiPitchC5+2, 8),
                 Note(MidiPitchC5+2, 8),
                 
                 Note(MidiPitchC5-1, 12),
                 Note(MidiPitchC5-3, 6),
                 Note(MidiPitchC5-1, 6),
                 
                 Note(MidiPitchC5-3, 3),
                 Note(MidiPitchC5-1, 3),
                 Note(MidiPitchC5, 3),
                 Note(MidiPitchC5+1, 3),
                 Note(MidiPitchC5+2, 6),
                 Note(MidiPitchC5, 6),
                 
                 Note(MidiPitchC5+1, 6),
                 Note(MidiPitchC5-2, 12),
                 Note(MidiPitchC5+1, 6),]];
    
    
    Voice *v5 = [[Voice alloc] initWithElements:@[
                 Note(MidiMiddleC-9, 3),
                 Note(MidiMiddleC-4, 3),
                 Note(MidiPitchD4-6, 6),
                 Note(MidiPitchC4-10, 6),
                 Note(MidiMiddleC+15, 3),
                 Note(MidiMiddleC+16, 3)]];
    
    Voice *v3 = [[Voice alloc] initWithElements:@[
                 Note(MidiPitchC6, 6),
                 Note(MidiPitchC4+1, 12),
                 Note(MidiPitchC4+3, 6),
                 Note(MidiPitchC4+2, 24)]];
    
    Voice *v4 = [[Voice alloc] initWithElements:@[
                 Note(MidiPitchG5 + 0, 6),
                 Note(MidiPitchG5 - 1, 3),
                 Note(MidiPitchG5 + 4, 3),
                 Note(MidiPitchG5 + 1, 12),
                 Note(MidiPitchG5 + 3, 4),
                 Note(MidiPitchG5+ 2, 4),
                 Note(MidiPitchG5 + 3, 4),
                 Note(MidiPitchG5 + 2, 6),
                 Note(MidiPitchG5 + 2, 6)]];
    
    Measure *m1 = [[Measure alloc] initWithVoices:@[v1]
                                             clef:TrebleClef
                                              key:KeyMajorCSharp
                                    timeSignature:TimeSignature4_4];
    
    Measure *m2 = [[Measure alloc] initWithVoices:@[v2]
                                             clef:TrebleClef
                                              key:KeyMajorC
                                    timeSignature:TimeSignature4_4];
    
    Measure *m3 = [[Measure alloc] initWithVoices:@[v3]
                                             clef:TrebleClef
                                              key:KeyMajorC
                                    timeSignature:TimeSignature4_4];
    
    Staff *staff = [[Staff alloc] initWithClef:TrebleClef
                                      measures:@[m1]
                                         frame:CGRectMake(0.0, 0.0, 600.0, 100.0)];
    
    Staff *staff2 = [[Staff alloc] initWithClef:TrebleClef
                                      measures:@[m2]
                                         frame:CGRectMake(0.0, 100.0, 600.0, 100.0)];
    CGContextTranslateCTM(ctx, 50.0, 0.0);
//    [staff draw:ctx];
    [staff2 draw:ctx];

}


@end
