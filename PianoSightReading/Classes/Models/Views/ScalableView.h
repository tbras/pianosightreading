//
//  ScalableView.h
//  PianoSightReading
//
//  Created by Tiago Bras on 25/10/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScalableView : UIView

@property(readonly) CGRect originalFrame;
@property(nonatomic) float scale;

- (void)draw: (CGContextRef)ctx;

- (id)initWithFrame:(CGRect)frame scale: (float)scale;

@end
