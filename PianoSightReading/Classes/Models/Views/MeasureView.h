//
//  MeasureView.h
//  PianoSightReading
//
//  Created by Tiago Bras on 25/10/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import "ScalableView.h"
#import "Measure.h"
#import "MusicNotationConfig.h"

@interface MeasureView : ScalableView

@property(nonatomic, strong, readonly) Measure *measure;
@property(nonatomic, assign) int number;
@property(nonatomic, strong) MusicNotationConfig *config;

- (id)initWithMeasure:(Measure *)measure
               number:(int)number
               config:(MusicNotationConfig *)config;

@end
