//
//  StaffView.m
//  PianoSightReading
//
//  Created by Tiago Bras on 25/10/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import "StaffView.h"
#import "TBUtils.h"
#import "Measure.h"
#import "Note.h"
#import "Voice.h"
#import "Element.h"
#import "NotationSymbols.h"
#import <CoreText/CoreText.h>
#import "Staff.h"

@implementation StaffView

@synthesize staff = _staff;
@synthesize config = _config;
@synthesize points;


- (id)initWithConfig:(MusicNotationConfig *)config frame: (CGRect)frame {
    float defHeight = config.staff.topSpace + config.staff.bottomSpace + (config.lines.spacing * 4.0);
//    float scale = floorf(((frame.size.height / defHeight) * 10.0)) / 10.0;

    float scale = 1.0;
    self = [super initWithFrame:frame scale:scale];
    if (self) {
        self.config = config;
        self.points = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)drawRulers: (CGContextRef)ctx {
    CGContextSaveGState(ctx);
    CGContextSetLineWidth(ctx, 0.5);
    CGContextSetShouldAntialias(ctx, NO);
    for (int i = 0; i <= self.bounds.size.width; i += 10) {
        CGContextMoveToPoint(ctx, 0.0, 0.0);
        CGContextAddLineToPoint(ctx, 0.0, self.bounds.size.height);
        CGContextStrokePath(ctx);
        CGContextTranslateCTM(ctx, 7.5, 0.0);
    }
    

    CGContextRestoreGState(ctx);
    
    CGContextSaveGState(ctx);
    CGContextSetShouldAntialias(ctx, NO);
    CGContextSetLineWidth(ctx, 0.5);
    
    for (int i = 0; i <= self.bounds.size.height; i += 10) {
        CGContextMoveToPoint(ctx, 0.0, 0.0);
        CGContextAddLineToPoint(ctx, self.bounds.size.width, 0.0);
        CGContextStrokePath(ctx);
        CGContextTranslateCTM(ctx, 0.0, 7.5);
    }
    
    CGContextRestoreGState(ctx);
}

- (void)drawMeasure: (CGContextRef)ctx {
    Note *n1 = [[Note alloc] initWithPitch:MidiMiddleC tick:6];
    Note *n2 = [[Note alloc] initWithPitch:MidiMiddleC tick:6];
    Note *n3 = [[Note alloc] initWithPitch:MidiMiddleC tick:6];
    Note *n4 = [[Note alloc] initWithPitch:MidiMiddleC tick:3];
    Note *n5 = [[Note alloc] initWithPitch:MidiMiddleC tick:3];
    
    Note *n6 = [[Note alloc] initWithPitch:MidiMiddleC tick:3];
    Note *n7 = [[Note alloc] initWithPitch:MidiMiddleC tick:12];
    Note *n8 = [[Note alloc] initWithPitch:MidiMiddleC tick:9];
    
    Note *n9 = [[Note alloc] initWithPitch:MidiMiddleC tick:2];
    Note *n10 = [[Note alloc] initWithPitch:MidiMiddleC tick:2];
    Note *n11 = [[Note alloc] initWithPitch:MidiMiddleC tick:2];
    Note *n12 = [[Note alloc] initWithPitch:MidiMiddleC tick:6];
    Note *n13 = [[Note alloc] initWithPitch:MidiMiddleC tick:12];
    
    [n1 setMinWidth:1.5];
    [n2 setMinWidth:1.5];
    [n3 setMinWidth:1.5];
    [n4 setMinWidth:1.5];
    [n5 setMinWidth:1.5];
    
    [n6 setMinWidth:1.5];
    [n7 setMinWidth:1.5];
    [n8 setMinWidth:1.5];
    
    [n9 setMinWidth:1.5];
    [n10 setMinWidth:1.5];
    [n11 setMinWidth:1.5];
    [n12 setMinWidth:1.5];
    [n13 setMinWidth:1.5];
    
    
    Voice *v1 = [[Voice alloc] initWithElements:@[n1, n2, n3, n4, n5]];
    Voice *v2 = [[Voice alloc] initWithElements:@[n6, n7, n8]];
    Voice *v3 = [[Voice alloc] initWithElements:@[n9, n10, n11, n12, n13]];
    
    Measure *measure = [[Measure alloc] initWithVoices:@[v1, v2, v3]
                                                  clef:TrebleClef
                                          key:KeyMajorC
                                         timeSignature:TimeSignature4_4];
    
    [measure layout:200.0];
    
    CTFontRef font2 = CTFontCreateWithName(CFSTR("Gonville-20"), 30, NULL);

    if (font2 == nil) {
        NSLog(@"Font not loaded.");
    }
    UIBezierPath *glyphBezierPath1 = [[NotationSymbols sharedInstance] bezierPathFromGlyph:Sym_noteheads_whole];
    
    CGContextSaveGState(ctx);
    int size = 30;
    for (Voice *v in [measure voices]) {
        
        for (Element *el in [v tickables]) {
            CGContextSaveGState(ctx);
            CGContextTranslateCTM(ctx, [el x], 0.0);
            
            
            [glyphBezierPath1 fill];
            CGContextRestoreGState(ctx);
        }
        
        CGContextTranslateCTM(ctx, 0.0, 10);
    }
    CGContextRestoreGState(ctx);
}

#define STAFF_TOP_SPACING 30.0
- (void)draw:(CGContextRef)ctx {
    float offset = 0.5;     // To be pixel perfect
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&
        ([UIScreen mainScreen].scale == 2.0)) {
        offset = 0.0;
    } else {
        offset = -0.5;
    }
    
    CGContextTranslateCTM(ctx, offset, offset);
    
    CGContextSetShouldAntialias(ctx, FALSE);
    
    float leftSpace = _config.staff.leftSpace;
    float rightSpace = _config.staff.rightSpace;
    float staffLength = (self.originalFrame.size.width / self.scale) - leftSpace - rightSpace;
    
    CGContextTranslateCTM(ctx, 0.0, STAFF_TOP_SPACING);
    
    CGContextSaveGState(ctx);
    
    float space = STAFF_LINE_SPACE;
    
    // First bar line
    CGContextSetLineWidth(ctx, 2.0);
    
    CGContextMoveToPoint(ctx, 0.0, 0.0);
    CGContextAddLineToPoint(ctx, 0.0, space * 4.0);
    CGContextStrokePath(ctx);
    
    // Draw last bar
    CGContextMoveToPoint(ctx, staffLength, 0.0);
    CGContextAddLineToPoint(ctx, staffLength, space * 4.0);
    CGContextStrokePath(ctx);
    
    printf("Spacing: %.2f\n", space);
    
    //    CGContextSetLineWidth(ctx, _config.lines.thickness);
    CGContextSetLineWidth(ctx, 1.0);
    for (int i = 0; i < 5; i++) {
        // Horizontal measure line
        CGContextMoveToPoint(ctx, 0.0, 0.0);
        CGContextAddLineToPoint(ctx, staffLength, 0.0);
        CGContextStrokePath(ctx);
        
        CGContextTranslateCTM(ctx, 0.0, space);
    }
    
    CGContextRestoreGState(ctx);
    
    CGContextSetShouldAntialias(ctx, TRUE);
//    
//    Note *n1 = [[Note alloc] initWithPitch:MidiMiddleC tick:6];
//    [n1 setLine:0];
//    [n1 setNoteHead:NoteHeadWhole];
//    
//    CGContextTranslateCTM(ctx, 20.0, 0.0);
//    [self drawNote:ctx note:n1 spacing:40.0];
//    
//    [n1 setLine:3];
//    [self drawNote:ctx note:n1 spacing:40.0];
//    
//    [n1 setLine:1];
//    [n1 setAccidental:AccidentalNatural];
//    [self drawNote:ctx note:n1 spacing:40.0];
//    
//    [n1 setLine:3];
//    [n1 setAccidental:AccidentalFlatFlat];
//    [n1 setNoteHead:NoteHeadQuarter];
//    [n1 setNoteFlag:NoteFlag8th];
//    [self drawNote:ctx note:n1 spacing:40.0];
//    
//    [n1 setLine:2];
//    [n1 setAccidental:AccidentalFlat];
//    [n1 setNoteHead:NoteHeadQuarter];
//    [n1 setNoteFlag:NoteFlag16th];
//    [self drawNote:ctx note:n1 spacing:40.0];
//    
//    [n1 setLine:-3];
//    [n1 setAccidental:AccidentalSharp];
//    [n1 setNoteHead:NoteHeadQuarter];
//    [n1 setNoteFlag:NoteFlag8th];
//    [n1 setMirror:YES];
//    [self drawNote:ctx note:n1 spacing:40.0];
//    
//    [n1 setLine:2];
//    [n1 setAccidental:AccidentalSharpSharp];
//    [n1 setNoteHead:NoteHeadHalf];
//    [n1 setNoteFlag:NoteFlagNone];
//    [n1 setMirror:YES];
//    [self drawNote:ctx note:n1 spacing:40.0];
//    
//    [n1 setLine:3];
//    [n1 setAccidental:AccidentalSharp];
//    [n1 setNoteHead:NoteHeadHalf];
//    [n1 setNoteFlag:NoteFlagNone];
//    [n1 setMirror:NO];
//    [self drawNote:ctx note:n1 spacing:40.0];
//    
//    Note *n2 = [[Note alloc] initWithPitch:MidiMiddleC tick:6];
//    [n2 setNoteHead:NoteHeadQuarter];
//    [n2 setAccidental:AccidentalNone];
//    [self drawNote:ctx note:n2 spacing:40.0];
//    
//    [n2 setNoteHead:NoteHeadQuarter];
//    [n2 setAccidental:AccidentalNone];
//    [n2 setNumOfDots:1];
//    [self drawNote:ctx note:n2 spacing:40.0];
//    
//    [n2 setNoteHead:NoteHeadQuarter];
//    [n2 setAccidental:AccidentalNone];
//    [n2 setNumOfDots:2];
//    [self drawNote:ctx note:n2 spacing:40.0];
//    
//    [n2 setNoteHead:NoteHeadQuarter];
//    [n2 setAccidental:AccidentalNone];
//    [n2 setNumOfDots:2];
//    [n2 setLine:1];
//    [self drawNote:ctx note:n2 spacing:40.0];
    
    Note *n1 = [[Note alloc] initWithPitch:MidiMiddleC tick:6];
    Note *n2 = [[Note alloc] initWithPitch:MidiPitchD4 tick:6];
    Note *n3 = [[Note alloc] initWithPitch:MidiPitchC4 tick:12];
//    Note *n4 = [[Note alloc] initWithPitch:MidiMiddleC tick:3];
//    Note *n5 = [[Note alloc] initWithPitch:MidiMiddleC tick:3];
//    
//    Note *n6 = [[Note alloc] initWithPitch:MidiMiddleC tick:3];
//    Note *n7 = [[Note alloc] initWithPitch:MidiMiddleC tick:12];
//    Note *n8 = [[Note alloc] initWithPitch:MidiMiddleC tick:9];
//    
//    Note *n9 = [[Note alloc] initWithPitch:MidiMiddleC tick:2];
//    Note *n10 = [[Note alloc] initWithPitch:MidiMiddleC tick:2];
//    Note *n11 = [[Note alloc] initWithPitch:MidiMiddleC tick:2];
//    Note *n12 = [[Note alloc] initWithPitch:MidiMiddleC tick:6];
//    Note *n13 = [[Note alloc] initWithPitch:MidiMiddleC tick:12];

    Voice *v1 = [[Voice alloc] initWithElements:@[n1, n2, n3]];
//    Voice *v2 = [[Voice alloc] initWithElements:@[n6, n7, n8]];
//    Voice *v3 = [[Voice alloc] initWithElements:@[n9, n10, n11, n12, n13]];
    
    Measure *m1 = [[Measure alloc] initWithVoices:@[v1]
                                                  clef:TrebleClef
                                                   key:KeyMajorC
                                         timeSignature:TimeSignature4_4];
    
    [m1 layout:90.0];
    CGContextTranslateCTM(ctx, 20.0, 0.0);
    [m1 draw:ctx];
    
    CGContextTranslateCTM(ctx, 20.0, 100.0);
    Staff *staff = [[Staff alloc] initWithClef:TrebleClef
                                      measures:@[m1]
                                         frame:CGRectMake(0.0, 0.0, 300.0, 100.0)];
    [staff draw:ctx];
    
//    for (int p = MidiMiddleC; p < MidiPitchG5; p++) {
//        [n2 setPitch:p];
//        [n2 update:KeyMajorC];
//        [self drawNote:ctx note:n2 spacing:15.0];
//    }

}

- (void)drawNote: (CGContextRef)ctx note:(Note*)note spacing: (float)spacing {
    CGContextSaveGState(ctx);
    
    CGContextTranslateCTM(ctx, 0.0, STAFF_LINE_SPACE * 2.0);
    
    [note draw:ctx];
    
    CGContextRestoreGState(ctx);
    
    CGContextTranslateCTM(ctx, spacing, 0.0);
}

- (float)setUpNotesInMeasure: (Measure *)measure desiredWidth: (float)width {
    
}

- (void)drawMeasures: (CGContextRef)ctx {
    
}


@end
