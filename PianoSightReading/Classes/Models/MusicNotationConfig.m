//
//  MusicNotationConfig.m
//  PianoSightReading
//
//  Created by Tiago Bras on 23/10/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import "MusicNotationConfig.h"
#import "NotationSymbols.h"

@implementation MusicNotationConfig

@synthesize lines = _lines;
@synthesize legderLines = _legderLines;
@synthesize measures = _measures;
@synthesize stems = _stems;
@synthesize beams = _beams;
@synthesize clefs = _clefs;
@synthesize keySignature = _keySignature;
@synthesize timeSignature = _timeSignature;
@synthesize staff = _staff;

+ (MusicNotationConfig *)sharedInstance {
    //  Static local predicate must be initialized to 0
    static MusicNotationConfig *sharedInstance = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[MusicNotationConfig alloc] init];
        
        [sharedInstance loadDefaults];
    });
    return sharedInstance;
}


- (void)loadDefaults {
    BOOL isRetina = TRUE;
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&
        ([UIScreen mainScreen].scale == 2.0)) {
        isRetina = TRUE;
        
        printf("Screen is retina.\n");
    } else {
        isRetina = FALSE;
        
        printf("Screen is not retina.\n");
    }
    
    _lines.spacing = STAFF_LINE_SPACE;
    _lines.thickness = isRetina ? 0.5 : 1.0;

    _legderLines.thickness = isRetina ? 2.0 : 1.0;
    _legderLines.leftHalfLength = _lines.spacing / 2.0;
    _legderLines.rightHalfLength = _lines.spacing / 2.0;
    
    _measures.displayBarLines = YES;
    _measures.displayFinalBarLine = YES;
    _measures.doubleBarSpacing = _lines.spacing * 0.5;
    _measures.thinLineThickness = isRetina ? 0.5 : 1.0;
    _measures.heavyLineThickness = isRetina ? 1.5 : 3.0;
    _measures.quarterNoteNormalDistance = _lines.spacing * 7.0;
    _measures.minItemsDistance = _lines.spacing * 2.5;
    _measures.minTiedNotesDistance = _lines.spacing * 3.0;
    _measures.leftSpacing = _lines.spacing * 1.2;
    _measures.rightSpacing = _lines.spacing * 1.2;
    
    _stems.thikness = isRetina ? 0.5 : 1.0;
    _stems.minHeight = _lines.spacing * 3.0;
    _stems.normalHeight = _lines.spacing * 4.0;
    
    _beams.thickness = isRetina ? 4.0 : 4.0;
    _beams.secondaryBeamSeparation = _lines.spacing * 0.3;
    _beams.maxDistanceFromMiddleStaffLine = 0.0;
    _beams.maxSlope = (_lines.spacing * 0.5) / (_measures.quarterNoteNormalDistance * 0.5);
    _beams.hookWidthCoef = 0.2;
    _beams.hookMinWidth = _lines.spacing;
    _beams.hookMaxWidth = _lines.spacing * 2.0;
    
    _clefs.leftSpacing = _lines.spacing * 0.1;
    _clefs.rightSpacing = _lines.spacing * 0.1;
    
    _keySignature.leftSpacing = _lines.spacing * .2;
    _keySignature.rightSpacing = _lines.spacing * .2;
    _keySignature.spacing = _lines.spacing * 1.3;   // FIXME: this is the width + space
    _keySignature.redisplayOnSignatureChange = TRUE;
    
    _timeSignature.leftSpacing = _lines.spacing * 1.0;
    _timeSignature.rightSpacing = _lines.spacing * 1.0;
    _timeSignature.topSpacing  = _lines.spacing * 0.1;
    _timeSignature.bottomSpacing = _lines.spacing * 0.1;
    _timeSignature.centerSpacing = _lines.spacing * 0.1;
    
    _staff.topSpace = _lines.spacing * 5.0;
    _staff.bottomSpace = _lines.spacing * 5.0;
    _staff.leftSpace = _lines.spacing * 1.0;
    _staff.rightSpace = _lines.spacing * 1.0;
    _staff.grandStaffSpacing = _lines.spacing * 4.0;

}

@end
