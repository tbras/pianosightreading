//
//  MusicNotationConfig.h
//  PianoSightReading
//
//  Created by Tiago Bras on 23/10/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import <Foundation/Foundation.h>

#define MNCONFIG [MusicNotationConfig sharedInstance]

typedef struct LinesConfig {
    float spacing;
    float thickness;
}LinesConfig;

typedef struct LedgerLinesConfig {
    float thickness;
    float leftHalfLength;
    float rightHalfLength;
}LedgerLinesConfig;

typedef struct MeasuresConfig {
    BOOL displayBarLines;       // Draw lines between bars (YES)
    BOOL displayFinalBarLine;   // Display the traditional end of piece bar line (YES)
    float thinLineThickness;    // Thickness of bar lines (1.0)
    float heavyLineThickness;   // Thicness of bar lines such as "Repeat bar line" (3.0)
    float doubleBarSpacing;     // Space between double bar lines (2.0)
    float quarterNoteNormalDistance;
    float minItemsDistance;
    float minTiedNotesDistance;
    float leftSpacing;
    float rightSpacing;
}MeasuresConfig;

typedef struct StemsConfig {
    float normalHeight;
    float minHeight;
    float thikness; // (1.0)
}StemsConfig;

typedef struct BeamsConfig {
    float thickness;
    float secondaryBeamSeparation;  // Space between beams in 16th+ notes (3.0)
    float maxSlope;
    float maxDistanceFromMiddleStaffLine;
    float hookWidthCoef;
    float hookMinWidth;
    float hookMaxWidth;
}BeamsConfig;

typedef struct ClefsConfig {
    float leftSpacing;
    float rightSpacing;
}ClefsConfig;

typedef struct KeySignatureConfig {
    float leftSpacing;
    float rightSpacing;
    float spacing;
    BOOL redisplayOnSignatureChange;
}KeySignatureConfig;

typedef struct TimeSignatureConfig {
    float leftSpacing;
    float rightSpacing;
    float topSpacing;
    float bottomSpacing;
    float centerSpacing;
}TimeSignatureConfig;

typedef struct StaffConfig {
    float topSpace;
    float bottomSpace;
    float leftSpace;
    float rightSpace;
    float grandStaffSpacing;
}StaffConfig;


@interface MusicNotationConfig : NSObject

@property(nonatomic, assign) LinesConfig lines;
@property(nonatomic, assign) LedgerLinesConfig legderLines;
@property(nonatomic, assign) MeasuresConfig measures;
@property(nonatomic, assign) StemsConfig stems;
@property(nonatomic, assign) BeamsConfig beams;
@property(nonatomic, assign) ClefsConfig clefs;
@property(nonatomic, assign) KeySignatureConfig keySignature;
@property(nonatomic, assign) TimeSignatureConfig timeSignature;
@property(nonatomic, assign) StaffConfig staff;

+ (MusicNotationConfig *)sharedInstance;

- (void)loadDefaults;

@end
