//
//  Note.m
//  PianoSightReading
//
//  Created by Tiago Bras on 25/10/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import "Note.h"
#import "Element.h"
#import "NotationSymbols.h"
#import "MusicNotationConfig.h"
#import "TheoryHelper.h"
#import "Beam.h"
#import <QuartzCore/QuartzCore.h>

#define DEF_VELOCITY 60

@interface Note()
- (void)drawLedgerLines: (CGContextRef)ctx;
- (void)drawHead: (CGContextRef)ctx;
- (void)drawStem: (CGContextRef)ctx;
- (void)drawFlag: (CGContextRef)ctx;
- (void)drawAccidentals: (CGContextRef)ctx;
- (void)drawDots: (CGContextRef)ctx;
@end

@implementation Note

@synthesize pitch = _pitch;
@synthesize velocity = _velocity;
@synthesize pitchOffset = _pitchOffset;

@synthesize noteHead = _noteHead;
@synthesize accidental = _accidental;
@synthesize articulation = _articulation;
@synthesize clef = _clef;
@synthesize numOfDots = _numOfDots;
@synthesize drawLedgersLines = _drawLedgersLines;

- (id)initWithPitch: (MidiPitch)pitch tick: (int)tick {
    return [[Note alloc] initWithPitch:pitch tick:tick velocity:DEF_VELOCITY];
}

- (id)initWithPitch: (MidiPitch)pitch tick: (int)tick velocity:(int)velocity {
    if ((self = [super init])) {
        [self setTick:tick];
        [self setMeasureTick:0];
        
        _pitch = pitch;
        _velocity = _velocity;
        _pitchOffset = 0;
        
        _noteHead = NoteHeadQuarter;
        _accidental = AccidentalNone;
        _articulation = ArticulationNone;
        _clef = TrebleClef;
        _numOfDots = 0;
        _drawLedgersLines = TRUE;
        
        [self setStemHeight:MNCONFIG.stems.normalHeight];
        [self setStemDirection:StemUpwards];
        
        [self setMinWidth:STAFF_LINE_SPACE * 2.0];
    }
    return self;
}

- (void)update: (Key)key {
    self.line = [TheoryHelper lineForPitch:_pitch key:key clef:_clef];
    self.stemDirection = self.line >= 0 ? StemDownwards : StemUpwards;
}

- (void)draw: (CGContextRef)ctx {
    CGContextSaveGState(ctx);
    {
        // Calculate xOffset so the stem/flag is draw correctly
        float xOffset;
        
        if (self.stemDirection == StemDownwards) {
            xOffset = 0.8;
        } else {
            xOffset = STAFF_LINE_SPACE + 1.6;
        }
        
        float diff = ((xOffset + [self x]) - (int)(xOffset + [self x]));
        
        if (diff > 0.50) {
            xOffset += 1.0 - diff;
        }
        
        // Draw ledgers lines
        if (_drawLedgersLines) {
            [self drawLedgerLines:ctx];
        }
        
        // Draw beams
        if (self.beams != nil || [self.beams count] > 0) {
            CGContextSaveGState(ctx);
            {
                CGContextSetShouldAntialias(ctx, TRUE);
                
                // Draw a rectangle beams
                float thickAndSepTotal = MNCONFIG.beams.secondaryBeamSeparation + MNCONFIG.beams.thickness;
                
                if (self.stemDirection == StemDownwards) {
                    thickAndSepTotal *= -1;
                    
                    CGContextTranslateCTM(ctx, 0.0, -MNCONFIG.beams.thickness);
                }
                
//                CGContextTranslateCTM(ctx, 0.0, [self.beam beamTier] * thickAndSepTotal);
                
//                for (int i = 0; i < [self.beam beamsCount]; i++) {
//                    CGContextMoveToPoint(ctx, self.x + xOffset, -self.measureStemEndY);
//                    CGContextAddLineToPoint(ctx, self.beam.toTickable.x + xOffset, -self.beam.toTickable.measureStemEndY);
//                    CGContextAddLineToPoint(ctx, self.beam.toTickable.x + xOffset, -self.beam.toTickable.measureStemEndY + MNCONFIG.beams.thickness);
//                    CGContextAddLineToPoint(ctx, self.x + xOffset, -self.measureStemEndY + MNCONFIG.beams.thickness);
//                    CGContextFillPath(ctx);
//                    
//                    CGContextTranslateCTM(ctx, 0.0, thickAndSepTotal);
//                }
                
                for (Beam *beam in self.beams) {
                    CGContextSaveGState(ctx);
                    
                    CGContextTranslateCTM(ctx, 0.0, beam.beamTier * thickAndSepTotal);
                    
                    for (int i = 0; i < [beam beamsCount]; i++) {
                        if (beam.beamDraw == BeamDrawForwardHook || beam.beamDraw == BeamDrawBackwardHook) {
                            float slope = (beam.toTickable.measureStemEndY - self.measureStemEndY) / (beam.toTickable.x - self.x);
                            
                            CGPoint start, end;
                            float hookWidth = MNCONFIG.beams.hookWidthCoef * (beam.toTickable.x - self.x);
                            
                            if (hookWidth > MNCONFIG.beams.hookMaxWidth) {
                                hookWidth = MNCONFIG.beams.hookMaxWidth;
                            } else if (hookWidth < MNCONFIG.beams.hookMinWidth) {
                                hookWidth = MNCONFIG.beams.hookMinWidth;
                            }
                            
                            if (self.beamType == BeamForwardHook) {
                                CGContextSaveGState(ctx);
                                
                                CGContextTranslateCTM(ctx, self.x + xOffset, -self.measureStemEndY);
                                CGContextMoveToPoint(ctx, 0.0, 0.0);
                                CGContextAddLineToPoint(ctx, hookWidth, -hookWidth * slope);
                                CGContextAddLineToPoint(ctx, hookWidth, -hookWidth * slope + MNCONFIG.beams.thickness);
                                CGContextAddLineToPoint(ctx, 0.0, MNCONFIG.beams.thickness);
                                CGContextFillPath(ctx);
                                
                                CGContextRestoreGState(ctx);
                            } else {
                                CGContextSaveGState(ctx);
                                
                                CGContextTranslateCTM(ctx, beam.toTickable.x + xOffset, -beam.toTickable.measureStemEndY);
                                CGContextMoveToPoint(ctx, 0.0, 0.0);
                                CGContextAddLineToPoint(ctx, 0.0, MNCONFIG.beams.thickness);
                                CGContextAddLineToPoint(ctx, -hookWidth, hookWidth * slope + MNCONFIG.beams.thickness);
                                CGContextAddLineToPoint(ctx, -hookWidth, hookWidth * slope);
                                CGContextFillPath(ctx);
                                
                                CGContextRestoreGState(ctx);
                            }
                            
//                            CGContextMoveToPoint(ctx, start.x, start.y);
//                            CGContextAddLineToPoint(ctx, end.x, end.y);
//                            CGContextAddLineToPoint(ctx, end.x, end.y + MNCONFIG.beams.thickness);
//                            CGContextAddLineToPoint(ctx, start.x, start.y + MNCONFIG.beams.thickness);
//                            CGContextFillPath(ctx);
                        } else {
                            CGContextMoveToPoint(ctx, self.x + xOffset, -self.measureStemEndY);
                            CGContextAddLineToPoint(ctx, beam.toTickable.x + xOffset, -beam.toTickable.measureStemEndY);
                            CGContextAddLineToPoint(ctx, beam.toTickable.x + xOffset, -beam.toTickable.measureStemEndY + MNCONFIG.beams.thickness);
                            CGContextAddLineToPoint(ctx, self.x + xOffset, -self.measureStemEndY + MNCONFIG.beams.thickness);
                            CGContextFillPath(ctx);
                        }
                        
                        CGContextTranslateCTM(ctx, 0.0, thickAndSepTotal);
                    }
                    
                    CGContextRestoreGState(ctx);
                }
                
//                CGContextMoveToPoint(ctx, self.x + xOffset, -self.measureStemEndY);
//                CGContextAddLineToPoint(ctx, self.beam.toTickable.x + xOffset,
//                                        -self.beam.toTickable.measureStemEndY);
//                CGContextSetLineWidth(ctx, MNCONFIG.beams.thickness);
//                CGContextStrokePath(ctx);

            }
            CGContextRestoreGState(ctx);
        }
        
        CGContextSaveGState(ctx);
        {
            // Translate to the note head position
            CGContextTranslateCTM(ctx, [self x], self.line * STAFF_LINE_SPACE_HALF * -1);
            
            // Draw stem only if it's not a whole note
            if (_noteHead != NoteHeadWhole) {
                CGContextSaveGState(ctx);
                {
                    CGContextSetShouldAntialias(ctx, FALSE);
                    
                    CGContextTranslateCTM(ctx, xOffset, 0.0);
                    
                    // Draw stem
                    [self drawStem:ctx];
                    
                    CGContextSetShouldAntialias(ctx, TRUE);
                    
                    // Draw flag
                    if ([self numOfFlags] > 0 && [self beamType] == BeamNone) {
                        [self drawFlag:ctx];
                    }
                }
                CGContextRestoreGState(ctx);
            }
            
            CGContextSetShouldAntialias(ctx, TRUE);
            
            // Draw note head
            [self drawHead:ctx];
            
            // Draw accidentals
            [self drawAccidentals:ctx];
            
            // Draw note dots
            [self drawDots:ctx];
        }
        CGContextRestoreGState(ctx);
        
    }
    CGContextRestoreGState(ctx);
}

#pragma mark - Private methods

- (void)drawLedgerLines: (CGContextRef)ctx {
    int numOfLedgersLines = (abs(self.line) - 4) / 2;
    numOfLedgersLines = numOfLedgersLines < 0 ? 0 : numOfLedgersLines;
    
    printf("Ledger linges: %d\n", numOfLedgersLines);
    
    float space = MNCONFIG.lines.spacing;
    
    CGContextSetLineWidth(ctx, 1.0);
    
    for (int i = 0; i < numOfLedgersLines; i++) {
        CGContextSaveGState(ctx);
        CGPoint p;
        
        if (self.line > 0) {
            p.x = [self x] - (space * 0.4);
            p.y = (i + 3) * -space;
        } else {
            p.x = [self x] - (space * 0.4);
            p.y = (i + 3) * space;
        }
        
        CGContextTranslateCTM(ctx, p.x, p.y);
        CGContextMoveToPoint(ctx, 0.0, 0.0);
        CGContextAddLineToPoint(ctx, space * 2, 0.0);
        CGContextStrokePath(ctx);
        
        CGContextRestoreGState(ctx);
    }
    
}

- (void)drawHead: (CGContextRef)ctx {
    [[NotationSymbols sharedInstance] drawGlyph:_noteHead];
}

- (void)drawBeam:(CGContextRef)ctx {
    
}

- (void)drawStem: (CGContextRef)ctx {
    CGContextSaveGState(ctx);
    
    CGContextMoveToPoint(ctx, 0.0, 0.0);
    
    if (self.stemDirection == StemDownwards) {
        CGContextTranslateCTM(ctx, 0.0, self.stemHeight);
    } else {
        CGContextTranslateCTM(ctx, 0.0, -self.stemHeight);
    }
    
    CGContextAddLineToPoint(ctx, 0.0, 0.0);
    CGContextSetLineWidth(ctx, 1.0);
    CGContextStrokePath(ctx);
    
    CGContextSetShouldAntialias(ctx, TRUE);
    
    CGContextRestoreGState(ctx);
}

- (void)drawFlag: (CGContextRef)ctx {
    CGContextSaveGState(ctx);
    
    float flagOffset = 0.5;
    
    if (self.stemDirection == StemDownwards) {
        CGContextTranslateCTM(ctx, flagOffset, self.stemHeight + flagOffset);
    } else {
        CGContextTranslateCTM(ctx, flagOffset, -self.stemHeight + flagOffset);
    }
    
    GlyphCode flag = Sym_space;
    
    switch ([self numOfFlags]) {
        case 1: flag = self.stemDirection == StemDownwards ? Sym_flags_d8 : Sym_flags_u8;
            break;
        case 2: flag = self.stemDirection == StemDownwards ? Sym_flags_d16 : Sym_flags_u16;
            break;
        case 3: flag = self.stemDirection == StemDownwards ? Sym_flags_d32 : Sym_flags_u32;
            break;
        case 4: flag = self.stemDirection == StemDownwards ? Sym_flags_d64 : Sym_flags_u64;
            break;
        default:
            break;
    }
    
    [[NotationSymbols sharedInstance] drawGlyph:flag];
    
    CGContextRestoreGState(ctx);
}

- (void)drawAccidentals: (CGContextRef)ctx {
    if (_accidental != AccidentalNone) {
        CGContextSaveGState(ctx);
        
        // Hack fix to draw the flat signs since there are inverted somehow
        if (_accidental == AccidentalFlat) {
            CGContextScaleCTM(ctx, 1.0, -1.0);
            CGContextTranslateCTM(ctx, -STAFF_LINE_SPACE*1.2, 0.0);
        } else if (_accidental == AccidentalFlatFlat) {
            CGContextScaleCTM(ctx, 1.0, -1.0);
            CGContextTranslateCTM(ctx, -STAFF_LINE_SPACE*1.9, 0.0);
        }
        else {
            CGContextScaleCTM(ctx, -1.0, 1.0);
            CGContextTranslateCTM(ctx, 2.5, 0.0);
        }
        
        GlyphCode acc = Sym_accidentals_sharp;
        
        switch (_accidental) {
            case AccidentalFlat: acc = Sym_accidentals_flat; break;
            case AccidentalFlatFlat: acc = Sym_accidentals_flatflat; break;
            case AccidentalNatural: acc = Sym_accidentals_natural; break;
            case AccidentalSharp: acc = Sym_accidentals_sharp; break;
            case AccidentalSharpSharp: acc = Sym_accidentals_sharpsharp; break;
            default:
                break;
        }
        
        [[[NotationSymbols sharedInstance] bezierPathFromGlyph:acc] fill];
        
        CGContextRestoreGState(ctx);
    }
}

- (void)drawDots: (CGContextRef)ctx {
    CGContextSaveGState(ctx);
    
    // If the note is on a line offset it by 3.0
    float yOffset = self.line % 2 ? 0.0 : -3.0;
    
    CGContextTranslateCTM(ctx, STAFF_LINE_SPACE + 5, yOffset);
    for (int i = 0; i < _numOfDots; i++) {
        [[NotationSymbols sharedInstance] drawGlyph:Sym_dot];
        
        CGContextTranslateCTM(ctx, 6.0, 0.0);
    }
    
    CGContextRestoreGState(ctx);
}


@end
