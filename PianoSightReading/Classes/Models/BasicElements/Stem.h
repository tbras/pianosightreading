//
//  Stem.h
//  PianoSightReading
//
//  Created by Tiago Bras on 10/01/13.
//  Copyright (c) 2013 Tiago Bras. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Stem : NSObject

@property(nonatomic, assign) float height;
@property(nonatomic, assign) CGPoint origin;



@end
