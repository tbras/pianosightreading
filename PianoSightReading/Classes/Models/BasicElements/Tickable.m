//
//  Tickable.m
//  PianoSightReading
//
//  Created by Tiago Bras on 13/01/13.
//  Copyright (c) 2013 Tiago Bras. All rights reserved.
//

#import "Tickable.h"
#import "MusicNotationConfig.h"
#import "Beam.h"

@implementation Tickable

@synthesize tick = _tick;
@synthesize measureTick = _measureTick;
@synthesize beamType = _beamType;
@synthesize numOfFlags = _numOfFlags;
@synthesize stemDirectionLocked = _stemDirectionLocked;
@synthesize beams = _beams;

- (id)init {
    if ((self = [super init])) {
        self.tick = 0;
        self.measureTick = 0;
        self.beamType = BeamNone;
        self.numOfFlags = 0;
        self.stemDirectionLocked = FALSE;
        self.beams = [[NSMutableArray alloc] init];
        
        [self setLine:0];
        [self setStemHeight:0.0];
        [self setStemDirection:StemUpwards];
    }
    
    return self;
}

- (void)addBeam: (Beam *)beam {
    [_beams addObject:beam];
}

- (int)line {
    return _line;
}

- (float)measureStemOriginY {
    return (MNCONFIG.lines.spacing * 0.5) * _line;
}

- (float)measureStemEndY {
    if (_stemDirection == StemUpwards) {
        return [self measureStemOriginY] + _stemHeight;
    } else {
        return [self measureStemOriginY] - _stemHeight;
    }
}

- (float)stemHeight {
    return _stemHeight;
}

- (StemDirection)stemDirection {
    return _stemDirection;
}

- (void)setLine:(int)line {
    _line = line;
}

- (void)setMeasureStemEndY: (float)y {
    float originY = [self measureStemOriginY];
    float newHeight = fabsf(y - originY);
    StemDirection newDirection = (y >= originY) ? StemUpwards : StemDownwards;
    
    if (_stemDirectionLocked == TRUE && newDirection != _stemDirection) {
        printf("Stem direction locked.\n");
    } else {
        _stemHeight = newHeight;
        _stemDirection = newDirection;
    }
}

- (void)setStemHeight: (float)height {
    _stemHeight = height;
}

- (void)setStemDirection: (StemDirection)direction {
    if (_stemDirectionLocked == FALSE) {
        _stemDirection = direction;
    } else {
        printf("Stem direction locked.\n");
    }
}

- (BOOL)isRest {
    return FALSE;
}

- (float)minHeight {
    return
    MNCONFIG.stems.minHeight +
        (MNCONFIG.beams.thickness * _numOfFlags) +
        (MNCONFIG.beams.secondaryBeamSeparation * (_numOfFlags - 1));
}

@end
