//
//  Chord.h
//  PianoSightReading
//
//  Created by Tiago Bras on 03/01/13.
//  Copyright (c) 2013 Tiago Bras. All rights reserved.
//

#import "Element.h"
#import "Note.h"

@interface Chord : Element

- (Note *)bottomNote;
- (Note *)topNote;

@end
