//
//  Beam.h
//  PianoSightReading
//
//  Created by Tiago Bras on 26/12/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Tickable.h"

typedef enum BeamDraw {
    BeamDrawLine,
    BeamDrawForwardHook,
    BeamDrawBackwardHook
}BeamDraw;

@interface Beam : NSObject

@property(nonatomic, strong) Tickable *toTickable;
@property(nonatomic, assign) int beamTier;  // 0 is the first beam tier (8th note)
@property(nonatomic, assign) int beamsCount;
@property(nonatomic, assign) BeamDraw beamDraw;

- (id)initWithBeamTier: (int)beamTier beamsCount: (int)beamsCount;

- (id)initWithTickable: (Tickable *)toTickable
              beamTier: (int)beamTier
            beamsCount: (int)beamsCount;

- (void)draw: (CGContextRef)ctx;

@end
