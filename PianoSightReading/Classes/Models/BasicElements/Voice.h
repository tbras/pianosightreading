//
//  Voice.h
//  PianoSightReading
//
//  Created by Tiago Bras on 28/11/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TheoryHelper.h"

@class Tickable;

@interface Voice : NSObject

@property(nonatomic, strong) NSMutableArray *tickables;

- (id)initWithElements: (NSArray *)tickables;

- (void)addElement: (Tickable *)tickable;

- (int)count;

- (int)totalTicks;
- (float)totalMinWidth;
- (float)totalWidth;

// First tickable is always zero, ex: For [2, 4, 2] - [@0, @2, @6, @8] 
- (NSArray *)accTickArray;

- (void)setupBeams: (TimeSignature)time;

- (void)draw: (CGContextRef)ctx;

@end
