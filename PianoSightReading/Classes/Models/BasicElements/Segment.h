//
//  Segment.h
//  PianoSightReading
//
//  Created by Tiago Bras on 29/11/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Voice;

@interface Segment : NSObject

@property(nonatomic, strong) NSMutableArray *voices;

// All tickables have ticks in the interval [inTick, outTick[
@property(nonatomic, assign) int inTick;
@property(nonatomic, assign) int outTick;

- (id)initWithVoices:(NSArray *)voices
              inTick:(int)inTick
             outTick:(int)outTick;

- (id)initWithInTicks:(int)inTick
              outTick:(int)outTick;;

- (void)addVoice: (Voice *)voice;
- (Voice *)voiceAtIndex: (int)index;
- (int)count;
- (int)deltaTick;
- (float)minWidth;

- (float)layout: (float)justifiedWidth xOffset:(float)xOffset;

@end
