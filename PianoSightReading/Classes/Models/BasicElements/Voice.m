//
//  Voice.m
//  PianoSightReading
//
//  Created by Tiago Bras on 28/11/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import "Voice.h"
#import "Tickable.h"
#import "BeamGroup.h"

@interface Voice()

@end

@implementation Voice

@synthesize tickables = _tickables;

- (id)init {
    if ((self = [super init])) {
        _tickables = [[NSMutableArray alloc] init];
    }
    return self;
}

- (id)initWithElements: (NSArray *)tickables {
    if ((self = [super init])) {
        _tickables = [[NSMutableArray alloc] initWithArray:tickables];
    }
    return self;
}

- (int)count {
    return [_tickables count];
}

- (void)addElement: (Tickable *)tickable {
    [_tickables addObject:tickable];
}

//- (NSArray *)elementsFrom: (int)firstIndex last:(int)lastIndex {
//    int size = [_elements count];
//    
//    if (size > 0 &&
//        firstIndex >= 0 &&
//        firstIndex <= lastIndex &&
//        lastIndex < size)
//    {
//        NSRange range = NSMakeRange(firstIndex, lastIndex - firstIndex + 1);
//        
//        return [_elements subarrayWithRange:range];
//    } else {
//        printf("Range [%d, %d] is not valid since elements' size = %d\n",
//               firstIndex, lastIndex - firstIndex + 1, size);
//        return nil;
//    }
//}

- (int)totalTicks {
    int ticksSum = 0;
    
    for (Tickable *t in _tickables) {
        ticksSum += [t tick];
    }
    
    return ticksSum;
}

- (float)totalMinWidth {
    float minWithSum = 0;
    
    for (Tickable *t in _tickables) {
        minWithSum += [t minWidth];
    }
    
    return minWithSum;
}

- (float)totalWidth {
    float withSum = 0;
    
    for (Tickable *t in _tickables) {
        withSum += [t width];
    }
    
    return withSum;
}

- (NSArray *)accTickArray {
    NSMutableArray *accTick = [[NSMutableArray alloc] initWithCapacity:[self count] + 1];
    int sum = 0;
    
    [accTick addObject:@0];
    for (Tickable *t in _tickables) {
        sum += [t tick];
        
        [accTick addObject:@(sum)];
    }
    
    return [[NSArray alloc] initWithArray:accTick];
}

- (void)printBeamTypes {
    printf("> ");
    for (Tickable *t in _tickables) {
        printf(" %3d (%d),", [t beamType], [t numOfFlags]);
    }
    printf("\n");
}

- (void)setupBeams: (TimeSignature)time {
    int quarterTick = (int)((float)([self totalTicks] / time.quantity) / (4.0 / time.quality));
    
    int count = [self count];
    
    if (count < 1) {
        return;
    }
    
    
    [self printBeamTypes];
    // 1st: all rests and notes >= 4th are set to NONE (ex: 4th note or rest), the rest is set to NOT_SET
    for (Tickable *t in _tickables) {
        if ([t isRest] || t.tick >= quarterTick) {
            [t setBeamType:BeamNone];
        } else {
            [t setBeamType:BeamNotSet];
        }
    }

    [self printBeamTypes];
    // 2nd pass: set to NONE all notes that are between 2 NONE notes
    if (count > 1 &&
        [(Tickable *)_tickables[1] beamType] == BeamNone) {
        [(Tickable *)_tickables[0] setBeamType:BeamNone];
    }
    
    if (count > 1 &&
        [(Tickable *)_tickables[count-2] beamType] == BeamNone) {
        [(Tickable *)_tickables[count-1] setBeamType:BeamNone];
    }
    
    for (int i = 1; i < count - 1; i++) {
        Tickable *curr = _tickables[i];
        Tickable *prev = _tickables[i-1];
        Tickable *next = _tickables[i+1];
        
        if (curr.beamType == BeamNone) {
            continue;
        }
        
        if (prev.beamType == BeamNone && next.beamType == BeamNone) {
            curr.beamType = BeamNone;
        }
    }
    
    [self printBeamTypes];
    // 3rd: all notes < 4th that go after a NONE and comes before the beat are set to NONE (ex: dotted 4th + 8th)
    // plus, all notes that are in the middle of 2 NONE notes
    for (int i = 1; i < count; i++) {
        Tickable *curr = _tickables[i];
        Tickable *prev = _tickables[i-1];
        
        if (curr.beamType == BeamNone) {
            continue;
        }
        
        if (((curr.measureTick + curr.tick) % quarterTick == 0) &&
            prev.beamType == BeamNone) {
            curr.beamType = BeamNone;
        }
    }
    
    [self printBeamTypes];
    // 4th pas: set START to all [NOT_SET] notes that come after a NONE and STOP to all [NOT_SET] notes that come before a NONE
    // the rest of [NOT_SET] set to CONTINUE
    if ([(Tickable *)_tickables[0] beamType] == BeamNotSet) {
        [(Tickable *)_tickables[0] setBeamType:BeamStart];
    }
    
    if ([(Tickable *)_tickables[count-1] beamType] == BeamNotSet) {
        [(Tickable *)_tickables[count-1] setBeamType:BeamStop];
    }
    
    for (int i = 1; i < count - 1; i++) {
        Tickable *curr = _tickables[i];
        Tickable *prev = _tickables[i-1];
        Tickable *next = _tickables[i+1];
        
        if (curr.beamType == BeamNone) {
            continue;
        }
        
        if (prev.beamType == BeamNone) {
            curr.beamType = BeamStart;
        } else if (next.beamType == BeamNone) {
            curr.beamType = BeamStop;
        } else {
            curr.beamType = BeamContinue;
        }
    }
    
    [self printBeamTypes];
    // 5th pass: correct some CONTINUE values
    for (int i = 2; i < count - 1; i++) {
        Tickable *curr = _tickables[i];
        Tickable *prev = _tickables[i-1];
        Tickable *prevprev = _tickables[i-2];
        
        if (curr.beamType != BeamContinue) {
            continue;
        }
        
        // All beats but the first
        if (curr.measureTick % quarterTick == 0 &&
            curr.measureTick > 0) {
            // If the last 2 notes were not 8th notes, set STOP-START
            if (!(prevprev.tick == quarterTick / 2 &&
                  prev.tick == quarterTick / 2)) {
                prev.beamType = BeamStop;
                curr.beamType = BeamStart;
            } else if ((curr.measureTick % (quarterTick * (time.quantity / 2))) &&
                       prev.beamType == BeamContinue) {
                prev.beamType = BeamStop;
                curr.beamType = BeamStart;
            }
        }
    }
    
    [self printBeamTypes];
    // 6h pass: set up hooks
    Tickable *first = (Tickable *)_tickables[0];
    if (count > 1 &&
        first.beamType == BeamStart &&
        first.tick < quarterTick / 2 &&
        first.numOfFlags > [(Tickable *)_tickables[1] numOfFlags]) {
        first.beamType = BeamForwardHook;
    }
    
    Tickable *last = (Tickable *)_tickables[count-1];
    if (count > 1 &&
        last.beamType == BeamStop &&
        last.tick < quarterTick / 2 &&
        last.numOfFlags > [(Tickable *)_tickables[count-2] numOfFlags]) {
        last.beamType = BeamBackwardHook;
    }
    
    for (int i = 1; i < count - 1; i++) {
        Tickable *curr = _tickables[i];
        Tickable *prev = _tickables[i-1];
        Tickable *next = _tickables[i+1];
        
        if (curr.beamType == BeamNone || curr.tick >= quarterTick / 2) {
            continue;
        }
        
        // Set a hook if the num of flags is bigger than its precedor or sucesor
        if (curr.beamType == BeamStart) {
            if (curr.numOfFlags > next.numOfFlags) {
                curr.beamType = BeamForwardHook;
            }
        } else if (curr.beamType == BeamContinue) {
            if (curr.numOfFlags > next.numOfFlags && curr.numOfFlags > prev.numOfFlags) {
                curr.beamType = BeamForwardHook;
            }
        } else {
            if (curr.numOfFlags > prev.numOfFlags) {
                curr.beamType = BeamBackwardHook;
            }
        }
    }
    
    [self printBeamTypes];
    NSMutableArray *beamGroups = [[NSMutableArray alloc] init];
    
    BeamGroup *group = nil;
    BOOL renew = TRUE;
    for (Tickable *t in _tickables) {
        if (t.beamType == BeamStart ||
            t.beamType == BeamForwardHook ||
            t.beamType == BeamBackwardHook ||
            t.beamType == BeamContinue ||
            t.beamType == BeamStop) {
            if (renew) {
                group = [[BeamGroup alloc] initWithQuarterNoteTick:quarterTick];
                renew = FALSE;
            }
            
            [group addTickable:t];
            
            if (t.beamType == BeamStop ||
                t.beamType == BeamBackwardHook) {
//                [beamGroups addObject:group];
                NSLog(@"Objec: %@", group);
                [group layout];
                renew = TRUE;
            }
        }
    }
    [self printBeamTypes];
//    return [[NSArray alloc] initWithArray:beamGroups];
}

- (void)draw: (CGContextRef)ctx {
    for (Tickable *t in _tickables) {
        [t draw:ctx];
    }
}

@end
