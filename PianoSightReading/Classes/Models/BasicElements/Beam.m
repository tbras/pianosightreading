//
//  Beam.m
//  PianoSightReading
//
//  Created by Tiago Bras on 26/12/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import "Beam.h"
#import "MusicNotationConfig.h"

@implementation Beam

@synthesize toTickable = _toTickable;
@synthesize beamTier = _beamTier;
@synthesize beamsCount = _beamsCount;
@synthesize beamDraw = _beamDraw;

- (id)initWithBeamTier: (int)beamTier beamsCount: (int)beamsCount {
    if ((self = [super init])) {
        self.toTickable = nil;
        self.beamTier = beamTier;
        self.beamsCount = beamsCount;
        self.beamDraw = BeamDrawLine;
    }
    
    return self;
}

- (id)initWithTickable: (Tickable *)toTickable
                  beamTier: (int)beamTier
                beamsCount: (int)beamsCount {
    if ((self = [super init])) {
        self.toTickable = toTickable;
        self.beamTier = beamTier;
        self.beamsCount = beamsCount;
    }
    
    return self;
}

- (void)draw: (CGContextRef)ctx {
//    MusicNotationConfig *c = [MusicNotationConfig sharedInstance];
//    float halfThickness = c.beams.thickness / 2.0;
//    
//    CGContextSaveGState(ctx);
//    CGContextTranslateCTM(ctx, 0.0, (_flagTier - 1) * c.beams.secondaryBeamSeparation);
//    
//    if (self.invertX) {
//        
//    }
//    
//    CGContextMoveToPoint(ctx, -0.5, -halfThickness);
//    CGContextAddLineToPoint(ctx, _length+1, (_length * _slope) - halfThickness);
//    CGContextAddLineToPoint(ctx, _length+1, (_length * _slope) + halfThickness);
//    CGContextAddLineToPoint(ctx, -0.5, halfThickness);
//    CGContextFillPath(ctx);
//    
//    CGContextRestoreGState(ctx);
}

@end
