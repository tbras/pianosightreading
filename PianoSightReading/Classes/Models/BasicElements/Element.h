//
//  MeasureElement.h
//  PianoSightReading
//
//  Created by Tiago Bras on 26/10/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Element : NSObject

@property(nonatomic, assign) float minWidth;
@property(nonatomic, assign) float width;
@property(nonatomic, assign) float x;

@property(nonatomic, assign) BOOL hidden;

// For example to add a clef change
@property(nonatomic, strong) Element *leftElement;

- (void)draw: (CGContextRef)ctx;


@end
