//
//  Segment.m
//  PianoSightReading
//
//  Created by Tiago Bras on 29/11/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import "Segment.h"
#import "Voice.h"
#import "Tickable.h"

@implementation Segment

@synthesize voices = _voices;
@synthesize inTick = _inTick;
@synthesize outTick = _outTick;

- (id)initWithVoices:(NSArray *)voices
              inTick:(int)inTick
             outTick:(int)outTick {
    if ((self = [super init])) {
        _voices = [[NSMutableArray alloc] initWithArray:voices];
        _inTick = inTick;
        _outTick = outTick;
    }
    return self;
}

- (id)initWithInTicks:(int)inTick
              outTick:(int)outTick {
    if ((self = [super init])) {
        _voices = [[NSMutableArray alloc] init];
        _inTick = inTick;
        _outTick = outTick;
    }
    return self;
}

- (void)addVoice: (Voice *)voice {
    [_voices addObject:voice];
}

- (Voice *)voiceAtIndex: (int)index {
    if ([self count] > 0 && index < [self count]) {
        return [_voices objectAtIndex:index];
    } else {
        return nil;
    }
}

- (int)count {
    return [_voices count];
}

- (int)deltaTick {
    return _outTick - _inTick;
}

- (float)minWidth {
    int size = [self count];
    
    if (size <= 0) {
        // FIXME: or not
        return 0.0;
    }
    
    float maxMinWidth = [_voices[0] totalMinWidth];
    for (int i = 1; i < size; i++) {
        float tmp = [_voices[i] totalMinWidth];
        if (tmp > maxMinWidth) {
            maxMinWidth = tmp;
        }
    }
    
    return maxMinWidth;
}

- (float)layout: (float)justifiedWidth xOffset:(float)xOffset {
    if ([self count] <= 0) {
        return xOffset + justifiedWidth;
    }
    
    int totalTick = _outTick - _inTick;

    for (Voice *v in _voices) {
        int spareTicks = totalTick;
        float spareWidth = justifiedWidth - [v totalMinWidth];
        
        float currX = xOffset;
        int accTick = 0;
        
        for (Tickable *t in [v tickables]) {
            int tick = [t tick];
            
            if (accTick + tick > totalTick) {
                [t setWidth:t.minWidth + spareWidth];
                [t setX:currX];
                
                break;
            } else {
                accTick += tick;
                
                // FIXME: watch out for "divided by 0 error"
                float addToWidth = ((float)tick / (float)spareTicks) * spareWidth;
                spareWidth -= addToWidth;
                spareTicks -= tick;
                
                if (spareWidth < 0.0) {
                    spareWidth = 0.0;
                    break;
                }
                
                [t setWidth:t.minWidth + addToWidth];
                [t setX:currX];
                
                currX += t.width;
            }
        }
    }
    
    return xOffset + justifiedWidth;
}

@end
