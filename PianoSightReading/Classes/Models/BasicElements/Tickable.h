//
//  Tickable.h
//  PianoSightReading
//
//  Created by Tiago Bras on 13/01/13.
//  Copyright (c) 2013 Tiago Bras. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Element.h"

@class Beam;

#define QUARTER_NOTE_DIVISIONS 48
#define NOTE_DURATION_4TH 48
#define NOTE_DURATION_8TH 24
#define NOTE_DURATION_16TH 12
#define NOTE_DURATION_32TH 6
#define NOTE_DURATION_64TH 3
#define NOTE_DURATION_4TH_TRIPLET 16

typedef enum BeamType {
    BeamNotSet,
    BeamNone,
    BeamStart,
    BeamContinue,
    BeamStop,
    BeamForwardHook,
    BeamBackwardHook
}BeamType;

typedef enum StemDirection {
    StemUpwards,
    StemDownwards
}StemDirection;

@interface Tickable : Element {
    float _stemX;
    float _stemHeight;
    StemDirection _stemDirection;
    int _line; // -4 = Bottom line, 4 = Top Line, Middle line = 0
}

@property(nonatomic, assign) int tick;
@property(nonatomic, assign) int measureTick;

@property(nonatomic, assign) BeamType beamType;
@property(nonatomic, assign) int numOfFlags;

@property(nonatomic, assign) BOOL stemDirectionLocked;

@property(nonatomic, strong) NSMutableArray *beams;

- (void)addBeam: (Beam *)beam;

- (void)drawStem:(CGContextRef)ctx;
- (void)drawBeam:(CGContextRef)ctx;

- (void)draw:(CGContextRef)ctx;

// Getters and setters
- (float)measureStemOriginY;
- (float)measureStemEndY;

- (int)line;
- (float)stemHeight;
- (StemDirection)stemDirection;

- (void)setMeasureStemEndY: (float)y;

- (void)setLine: (int)line;
- (void)setStemHeight: (float)height;
- (void)setStemDirection: (StemDirection)direction;

- (BOOL)isRest;

- (float)minHeight;

@end
