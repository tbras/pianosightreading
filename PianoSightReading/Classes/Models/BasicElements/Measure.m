//
//  Measure.m
//  PianoSightReading
//
//  Created by Tiago Bras on 25/10/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import "Measure.h"
#import "Tickable.h"
#import "Voice.h"
#import "Segment.h"
#import "Note.h"
#import "MusicNotationConfig.h"
#import "BeamGroup.h"

@interface Measure()

- (void)updateMeasureTicks;
- (void)updateTickables;
- (void)setNoteHeadAndDots: (Note *)note totalTick: (int)totalTick;
- (float)calcNonTickableSpaceNeeded;
- (float)calcClefWidth;
- (float)calcTimeSignatureWidth;
- (float)calcKeySignatureWidth;

- (void)drawKey: (CGContextRef)ctx;
@end

@implementation Measure

@synthesize voices = _voices;

@synthesize clef = _clef;
@synthesize key = _key;
@synthesize timeSignature = _timeSignature;

@synthesize drawClef = _drawClef;
@synthesize drawKeySignature = _drawKeySignature;
@synthesize drawTimeSignature = _drawTimeSignature;
@synthesize beamGroups = _beamGroups;

- (id)initWithVoices: (NSArray *)voices
                clef: (Clef)clef
                 key: (Key)key
       timeSignature: (TimeSignature)timeSignature {
    self = [super init];
    if (self) {
        _clef = clef;
        _key = key;
        _timeSignature = timeSignature;
        
        _drawClef = FALSE;
        _drawKeySignature = FALSE;
        _drawTimeSignature = FALSE;
        _beamGroups = [[NSArray alloc] init];
        
        _voices = [[NSMutableArray alloc] initWithArray:voices];
    }
    return self;
}

- (id)initWithClef: (Clef)clef
               key: (Key)key
     timeSignature: (TimeSignature)timeSignature
{
    self = [super init];
    if (self) {
        _clef = clef;
        _key = key;
        _timeSignature = timeSignature;
        
        _drawClef = FALSE;
        _drawKeySignature = FALSE;
        _drawTimeSignature = FALSE;
        _beamGroups = [[NSArray alloc] init];
        
        _voices = [[NSMutableArray alloc] initWithCapacity:4];
    }
    return self;
}


#pragma mark -
#pragma Public Methods
- (int)countVoices {
    return [_voices count];
}

- (void)addVoice: (Voice *)voice {
    [_voices addObject:voice];
}

- (BOOL)validateVoices {
    if ([self countVoices] <= 0) {
        return TRUE;
    }
    int expectedTicks = [_voices[0] totalTicks];
    
    for (Voice *v in _voices) {
        if (expectedTicks != [v totalTicks]) {
            return FALSE;
        }
    }
    
    return TRUE;
}

- (float)totalMinWidth {
    int totalVoices = [self countVoices];
    
    if (totalVoices <= 0) {
        return 0.0;
    } else {
        float max = [_voices[0] totalMinWidth];
        
        for (int i = 1; i < totalVoices; i++) {
            float tmp = [_voices[i] totalMinWidth];
            
            if (tmp > max) {
                max = tmp;
            }
        }
        
        return max;
    }
}

- (NSArray *)alignedAccTickablesIndexes:(NSArray *)acc1 acc2:(NSArray *)acc2 {
    int count1 = [acc1 count];
    int count2 = [acc2 count];
    int i = 0;
    int j = 0;
    
    assert((count1 > 0) && (count2 > 0));
    
    NSMutableArray *matches = [[NSMutableArray alloc] init];
    
    @autoreleasepool {
        while (i < count1) {
            while (j < count2) {
                int a = [acc1[i] intValue];
                int b = [acc2[j] intValue];
                
                if (a == b) {
                    [matches addObject:[[AlignedTickable alloc] initWithTick:a
                                                                      indexA:i
                                                                      indexB:j]];
                    j++;
                    break;
                } else if (a < b) {
                    break;
                } else {
                    j++;
                }
            } // INNER while
            i++;
        } // OUTTER while
    }
    
    return [NSArray arrayWithArray:matches];
}

- (NSArray *)alignedTickablesIndexes: (Voice *)v1 v2:(Voice *)v2 {
    return nil;
}

- (float)layout: (float)justifiedWidth {
    // Update Element.measureTick
    [self updateMeasureTicks];
    
    // Update tickables lines position, etc...
    [self updateTickables];
    
    // Space needed by other elements such as padding, time signature
    float space = [self calcNonTickableSpaceNeeded];
    
    if (justifiedWidth < [self totalMinWidth] + space || [self validateVoices] == FALSE) {
        assert(FALSE);
        return justifiedWidth;
    }
    
    float tickablesWidth = justifiedWidth - space;
    
    int voicesLen = [self countVoices];
    
    // Caso especial se houver apenas uma voz
    if (voicesLen == 1) {
        Segment *seg = [[Segment alloc] initWithInTicks:0
                                                outTick:[_voices[0] totalTicks]];
        
        [seg addVoice:_voices[0]];
        [seg layout:tickablesWidth xOffset:0.0];
        
        for (Voice *v in _voices) {
            [v setupBeams:_timeSignature];
        }
        
//        for (BeamGroup *bg in _beamGroups) {
//            [bg layout];
//        }
        
        return justifiedWidth;
    }
    
    NSMutableArray *accTickables = [[NSMutableArray alloc] initWithCapacity:voicesLen];
    
    for (Voice *v in _voices) {
        [accTickables addObject:[v accTickArray]];
    }
    
    NSMutableArray *alignedArray = [[NSMutableArray alloc] initWithCapacity:voicesLen];
    
    // Calculate aligned tickables
    // Ex, for 4 voices we have:
    // (0, 1), (0, 2), (0, 3), (1, 2), (1, 3), (2, 3)
    for (int i = 0; i < voicesLen - 1; i++) {
        for (int j = i + 1; j < voicesLen; j++) {
            NSArray *acc1 = accTickables[i];
            NSArray *acc2 = accTickables[j];
            
            [alignedArray addObject:[self alignedAccTickablesIndexes:acc1 acc2:acc2]];
        }
    }
    
    // Calculate the number of segments by the calculating the unique acc tick values
    NSMutableArray *uniqueValues = [[NSMutableArray alloc] init];
    
    for (NSArray *arr in alignedArray) {
        for (AlignedTickable *t in arr) {
            BOOL exists = FALSE;
            for (NSNumber *n in uniqueValues) {
                if ([n intValue] == [t tick]) {
                    exists = TRUE;
                    break;
                }
            }
            
            // If it doesn't exist, add.
            if (exists == FALSE) {
                [uniqueValues addObject:@([t tick])];
            }
        }
    }
    
    // Sort Unique values array
    [uniqueValues sortUsingComparator:^(id obj1, id obj2) {
        return [(NSNumber *)obj1 compare:(NSNumber *)obj2];
    }];
    
    // Ex: [0, 4, 8, 20] - Segments = (0-4), (4-8), (8-20)
    int numOfSegments = [uniqueValues count] - 1;
    int maxTicks = [uniqueValues[numOfSegments] intValue];
    
    NSMutableArray *segmentsArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < numOfSegments; i++) {
        int inTick = [uniqueValues[i] intValue];
        int outTick = [uniqueValues[i+1] intValue];
        //        float weight = (float)(outTick - inTick) / (float)maxTicks;
        
        Segment *seg = [[Segment alloc] initWithInTicks:inTick
                                                outTick:outTick];
        
        [segmentsArray addObject:seg];
    }
    
    // Fill the segments array
    for (Segment *seg in segmentsArray) {
        for (int i = 0; i < voicesLen; i++) {
            Voice *v = [[Voice alloc] init];
            
            for (int j = 0; j < [accTickables[i] count] - 1; j++) {
                int accTick = [accTickables[i][j] intValue];
                
                if (accTick >= [seg inTick] && accTick < [seg outTick]) {
                    [v addElement:[_voices[i] tickables][j]];
                } else if (accTick >= [seg outTick]) {
                    break;
                } else {
                    continue;
                }
            }
            
            [seg addVoice:v];
        }
    }
    
    // Calculate all seg minWidth
    NSMutableArray *segMinWidths = [[NSMutableArray alloc]initWithCapacity:numOfSegments];
    float segTotalMinWidth = 0.0;
    
    for (Segment *seg in segmentsArray) {
        float tmp = [seg minWidth];
        
        [segMinWidths addObject:@(tmp)];
        
        segTotalMinWidth += tmp;
    }
    
    float currX = 0.0;
    float spareWidth = tickablesWidth - segTotalMinWidth;
    int spareTicks = maxTicks;
    for (int i = 0; i < numOfSegments; i++) {
        Segment *seg = segmentsArray[i];
        
        float widthToAdd = (((float)[seg deltaTick] / (float)spareTicks) * spareWidth);
        float jw = widthToAdd + [segMinWidths[i] floatValue];
        
        spareWidth -= widthToAdd;
        spareTicks -= [seg deltaTick];
        
        currX = [seg layout:jw xOffset:currX];
    }
    
    for (Voice *v in _voices) {
        [v setupBeams:_timeSignature];
    }
    
//    for (BeamGroup *bg in _beamGroups) {
//        [bg layout];
//    }
    
    // Setup beams
    
    return justifiedWidth;
}

- (void)draw: (CGContextRef)ctx {
    MusicNotationConfig *config = [MusicNotationConfig sharedInstance];
    
    CGContextSaveGState(ctx);
    
    CGContextTranslateCTM(ctx, config.measures.leftSpacing, 0.0);
    
    CGContextSetShouldAntialias(ctx, TRUE);
    if (_drawClef) {
        CGContextSaveGState(ctx);
        CGContextTranslateCTM(ctx, config.clefs.leftSpacing, config.lines.spacing);
        
        [[NotationSymbols sharedInstance] drawGlyph:[TheoryHelper clefGlyph:_clef]];
        
        CGContextRestoreGState(ctx);
        CGContextTranslateCTM(ctx, [self calcClefWidth], 0.0);
    }
    
    if (_drawKeySignature) {
        [self drawKey:ctx];
        CGContextTranslateCTM(ctx, [self calcKeySignatureWidth], 0.0);
    }
    
    for (Voice *v in _voices) {
        [v draw:ctx];
    }
    
    CGContextRestoreGState(ctx);
}

#pragma mark - Private Methods

- (void)updateMeasureTicks {
    for (Voice *voice in _voices) {
        int measureTick = 0;
        for (Tickable *tickable in [voice tickables]) {
            [tickable setMeasureTick:measureTick];
            measureTick += [tickable tick];
        }
    }
}

- (void)setNoteHeadAndDots: (Note *)note totalTick: (int)totalTick {
    int noteValuetick = totalTick / _timeSignature.quantity;
    
    if (_timeSignature.quality == 2) {
        noteValuetick /= 2;
    }
    
    NoteHead noteHead;
    int q = [note tick] / noteValuetick;
    
    // q == 0 it's at least a 8th note
    if (q == 0) {
        noteHead = NoteHeadQuarter;
        
        if (noteValuetick / [note tick] <= 3) {
            [note setNumOfFlags:1];
        } else if (noteValuetick / [note tick] == 4) {
            [note setNumOfFlags:2];
        } else if (noteValuetick / [note tick] == 8) {
            [note setNumOfFlags:3];
        } else if (noteValuetick / [note tick] == 16) {
            [note setNumOfFlags:4];
        }
    } else if (q == 1) {
        noteHead = NoteHeadQuarter;
    } else if (q > 1 && q < 4) {
        noteHead = NoteHeadHalf;
    } else {
        noteHead = NoteHeadWhole;
    }
    
    [note setNoteHead:noteHead];
    
    
    int numOfDots = 0;
    int r = 0;
    
    // q = 0 (<= 8th note), q > 0 (>= 4th)
    if (q == 0) {
        r = noteValuetick % [note tick];
    } else {
        r = [note tick] % noteValuetick;
    }
    
    if (r == (noteValuetick / 4)) {
        numOfDots = 1;
    } else if (r == ((noteValuetick / 4) + (noteValuetick / 8))) {
        numOfDots = 2;
    }
    
    [note setNumOfDots:numOfDots];
}

- (void)updateTickables {
    float offset = 0.0;
    
    for (Voice *voice in _voices) {
        int totalTicks = [voice totalTicks];
        NSArray *tickables = [voice tickables];
        int size = [tickables count];
        
        for (int i = 0; i < size; i++) {
            Element *element = tickables[i];
            
            // Set note head type and dots, and update line
            if ([element isKindOfClass:[Note class]]) {
                [self setNoteHeadAndDots:(Note *)element totalTick:totalTicks];
                
                [(Note *)element update:_key];
            }
            
            float addToMinWidth = 0.0;
            if ([element respondsToSelector:@selector(accidental)]) {
                switch ([(Note *)element accidental]) {
                    case AccidentalFlat:
                    case AccidentalSharp:
                    case AccidentalSharpSharp:
                    case AccidentalNatural:     addToMinWidth = STAFF_LINE_SPACE * 3.0;
                        break;
                    case AccidentalFlatFlat:    addToMinWidth = STAFF_LINE_SPACE * 4.5;
                    case AccidentalNone:        addToMinWidth = 0.0;
                    default:
                        addToMinWidth = 0.0;
                        break;
                }
                
                // Especial case, first element
                if (i == 0) {
                    if (addToMinWidth > offset) {
                        offset = addToMinWidth;
                    }
                } else {
                    Element *prev = (Element *)tickables[i-1];
                    
                    [prev setMinWidth:[prev minWidth] + addToMinWidth];
                }
                
                // Add compensation for the dots
                if ([(Note *)element respondsToSelector:@selector(numOfDots)]) {
                    float dotsWidth = STAFF_LINE_SPACE_HALF * [(Note *)element numOfDots];
                    
                    if (dotsWidth > 0.0) {
                        [element setMinWidth:[element minWidth] + dotsWidth];
                    }
                }
            }
        }
    }
}

- (float)calcNonTickableSpaceNeeded {
    MusicNotationConfig *config = [MusicNotationConfig sharedInstance];
    
    float space = config.measures.leftSpacing + config.measures.rightSpacing;
    
    if (_drawClef) {
        space += [self calcClefWidth];
    }
    
    if (_drawKeySignature) {
        space += [self calcKeySignatureWidth];
    }
    
    if (_drawTimeSignature) {
        space += [self calcTimeSignatureWidth];
    }

    return space;
}

- (float)calcClefWidth {
    MusicNotationConfig *c = [MusicNotationConfig sharedInstance];
    
    float width =  c.clefs.leftSpacing + c.clefs.rightSpacing + (c.lines.spacing * 3.0);
    
    if (_drawClef) {
        return width;
    } else {
        return 0.0;
    }
}

- (float)calcKeySignatureWidth {
    MusicNotationConfig *c = [MusicNotationConfig sharedInstance];
    
    float width = abs([TheoryHelper signatureForKey:_key]) * (c.keySignature.spacing);
    
    if (_drawKeySignature) {
        return width + c.keySignature.leftSpacing + c.keySignature.rightSpacing;
    } else {
        return 0.0;
    }
}

- (float)calcTimeSignatureWidth {
    MusicNotationConfig *c = [MusicNotationConfig sharedInstance];
    
    float width = c.lines.spacing * 3.0;
    
    if (_drawTimeSignature) {
        return width + c.timeSignature.leftSpacing + c.timeSignature.rightSpacing;
    } else {
        return 0.0;
    }
}

- (void)drawKey: (CGContextRef)ctx {
    MusicNotationConfig *c = [MusicNotationConfig sharedInstance];
    
    CGContextSaveGState(ctx);
    int accidentals = [TheoryHelper signatureForKey:_key];
    
    GlyphCode code = accidentals < 0 ? Sym_accidentals_flat : Sym_accidentals_sharp;
    
    float drawSharps = code == Sym_accidentals_sharp ? TRUE : FALSE;
    
    int trebleFlatsOrder[7] = {0, 3, -1, 2, -2, 1, -3};
    int trebleSharpsOrder[7] = {4, 1, 5, 2, -1, -3, 0};
    
    if (_clef == BassClef) {
        CGContextTranslateCTM(ctx, 0.0, c.lines.spacing);
    } else if (_clef == AltoClef) {
        CGContextTranslateCTM(ctx, 0.0, c.lines.spacing / 2.0);
    }
    
    CGContextTranslateCTM(ctx, c.keySignature.leftSpacing, 0.0);
    
    assert(abs(accidentals) <= 7);
    for (int i = 0; i < abs(accidentals); i++) {
        if (drawSharps) {
            CGContextSaveGState(ctx);
            CGContextTranslateCTM(ctx, 0.0, -trebleSharpsOrder[i] * (c.lines.spacing / 2.0));
            [[NotationSymbols sharedInstance] drawGlyph:code];
            CGContextRestoreGState(ctx);
            
            CGContextTranslateCTM(ctx, c.keySignature.spacing, 0.0);
        }
    }
    
    CGContextRestoreGState(ctx);
}


@end

#pragma mark -
#pragma Helper class AlignedTickable

@implementation AlignedTickable

@synthesize tick = _tick;
@synthesize indexA = _indexA;
@synthesize indexB = _indexB;

- (id)initWithTick: (int)tick indexA:(int)indexA indexB:(int)indexB {
    if ((self = [super init])) {
        _tick = tick;
        _indexA = indexA;
        _indexB = indexB;
    }
    return self;
}
    


@end
