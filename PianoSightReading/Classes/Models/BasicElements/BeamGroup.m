//
//  Beam.m
//  PianoSightReading
//
//  Created by Tiago Bras on 23/12/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import "BeamGroup.h"
#import "MusicNotationConfig.h"
#import "Beam.h"

@interface BeamGroup()
- (void)drawBeam: (CGContextRef)ctx startPoint:(CGPoint)sp endPoint:(CGPoint)ep;

- (StemDirection)calculateStemDirection;
- (Tickable *)topTickable;
- (Tickable *)bottomTickable;
- (Tickable *)pivotTickable;
- (float)calculateSlope;

- (int)minNumOfFlags;
- (int)maxNumOfFlags;

@end

@implementation BeamGroup

@synthesize tickables = _tickables;
@synthesize quarterNoteTick = _quarterNoteTick;
@synthesize stemDirection = _stemDirection;
@synthesize stemDirectionLocked = _stemDirectionLocked;

- (id)initWithQuarterNoteTick: (int)quarterNoteTick {
    if ((self = [super init])) {
        self.tickables = [[NSMutableArray alloc] init];
        self.quarterNoteTick = quarterNoteTick;
        self.stemDirection = StemUpwards;
        self.stemDirectionLocked = FALSE;
    }
    
    return self;
}

- (id)initWithTickables: (NSArray *)tickables quarterNoteTick: (int)quarterNoteTick {
    if ((self = [super init])) {
        self.tickables = [[NSMutableArray alloc] initWithArray:tickables];
        self.quarterNoteTick = quarterNoteTick;
        self.stemDirection = StemUpwards;
        self.stemDirectionLocked = FALSE;
    }
    
    return self;
}

- (void)addTickable: (Element *)element {
    [self.tickables addObject:element];
}

- (void)layout {
    int count = [_tickables count];
    
    assert(count >= 2);
    
    if (_stemDirectionLocked == FALSE) {
        _stemDirection = [self calculateStemDirection];
    }
    
    Tickable *pivot = [self pivotTickable];
    [pivot setStemHeight:[pivot minHeight]];
    [pivot setStemDirection:_stemDirection];
    
    float slope = [self calculateSlope];
    
    // Calculate new tickables height
    for (Tickable *t in _tickables) {
        if (t != pivot) {
            [t setStemHeight:[t minHeight]];
            [t setStemDirection:_stemDirection];
            
            float newMeasureEndY = ((t.x - pivot.x) * slope) + ([pivot measureStemEndY]);
            
            [t setMeasureStemEndY:newMeasureEndY];
        }
    }
    
    
    int minNumOfFlags = [self minNumOfFlags];
    int maxNumOfFlags = [self maxNumOfFlags];
    
    
    // Add first beam
    if (minNumOfFlags == maxNumOfFlags) {
        Beam *beam = [[Beam alloc] initWithTickable:_tickables[count - 1]
                                           beamTier:0
                                         beamsCount:minNumOfFlags];
        [_tickables[0] addBeam:beam];
    } else {
        Beam *beam = [[Beam alloc] initWithTickable:_tickables[count - 1]
                                           beamTier:0
                                         beamsCount:1];
        [_tickables[0] addBeam:beam];
        
        
        // Set beams
        for (int i = 2; i <= maxNumOfFlags; i++) {
            Tickable *first = nil;
            Tickable *last = nil;
            
            for (int j = 0; j < count; j++) {
                Tickable *t = _tickables[j];
                
                if (t.beamType == BeamForwardHook) {
                    assert((j >= 0) && (j < count - 1));
                    
                    first = t;
                    last = _tickables[j+1];
                    
                    Beam *beam = [[Beam alloc] initWithTickable:last
                                                       beamTier:i-1
                                                     beamsCount:1];
                    [beam setBeamDraw:BeamDrawForwardHook];
                    [first addBeam:beam];
                    
                    
                    first = nil;
                    last = nil;
                } else if (t.beamType == BeamBackwardHook) {
                    assert((j > 0) && (j < count));
                    
                    first = _tickables[j-1];
                    last = t;
                    
                    Beam *beam = [[Beam alloc] initWithTickable:last
                                                       beamTier:i-1
                                                     beamsCount:1];
                    [beam setBeamDraw:BeamDrawBackwardHook];
                    [first addBeam:beam];
                    
                    first = nil;
                    last = nil;
                } else if (first == nil && t.numOfFlags >= i) {
                    first = t;
                } else if (first != nil && t.numOfFlags < i) {
                    last = _tickables[j-1];
                    
                    if (first != last) {
                        int beamTier = last.numOfFlags - i + 1;
                        int beamsCount = 0;
                        
                        for (int k = 0; k < [first.beams count]; k++) {
                            beamsCount += [(Beam *)first.beams[k] beamsCount];
                        }
                        
                        
                        Beam *beam = [[Beam alloc] initWithTickable:last
                                                           beamTier:beamsCount
                                                         beamsCount:last.numOfFlags - beamsCount];
                        
                        [first addBeam:beam];
                    }
                    
                    first = nil;
                    last = nil;
                } else {
                    
                }
            }
            
            first = nil;
            last = nil;
        }
    }
    
//    Tickable *first = _tickables[0];
//    Tickable *last = _tickables[count - 1];
//    
//    NSLog(@"First: %@, x: %.2f", first, first.x);
//    NSLog(@"Last: %@, x: %.2f", last, last.x);
//    Beam *b = [[Beam alloc] initWithTickable:last beamTier:0 beamsCount:3];
//    [first setBeam:b];
    
//    BOOL mirror = _mirrored;
//    
//    if (_lockOrientation == FALSE) {
//        mirror = [self isGroupMirrored];
//    }
    
//    Tickable *pivot = mirror ? [self bottomElement] : [self topElement];
//    float pivotHeight = fabsf(MNCONFIG.stems.minHeight + ((pivot.numOfFlags - 1) * (MNCONFIG.beams.secondaryBeamSeparation + MNCONFIG.beams.thickness)));
//    float xOffset = [pivot x];
//    float slope = [self calcSlope];
//    
//    
//    for (Element *element in _elements) {
//        Note *note = nil;
//        if ([element isKindOfClass:[Note class]]) {
//            note = (Note *)element;
//        } else if ([element isKindOfClass:[Chord class]]) {
//            note = mirror ? [(Chord *)element bottomNote] : [(Chord *)element topNote];
//        } else {
//            continue;
//        }
//    
//        [note setMirror:mirror];
//    
//        float newHeight = ((note.x - xOffset) * slope) +
//        (abs(pivot.line - note.line) * (config.lines.spacing / 2.0)) + pivotHeight;
//    
//        [note setStemHeight:newHeight];
//        [note updateStemPoints];
//    }

    //    NSMutableArray *beams = [[NSMutableArray alloc] init];
    //
    //    // There is always a beam
    //
    //
    //    int numOfFlags = 2;
    //
    //    for (int i = 0; i < count; i++) {
    //        
    //    }
    
}

//- (void)layout {
//    assert([_elements count] > 0);
//    
//    MusicNotationConfig *config = [MusicNotationConfig sharedInstance];
//
//    BOOL mirrored = [self isGroupMirrored];
//    
//    Element *pivot = mirrored ? [self bottomElement] : [self topElement];
//    float pivotHeight = fabsf([self calcPivotHeight]);
//    float xOffset = [pivot x];
//    float slope = [self calcSlope];
//    
//    Note *n1 = nil;
//    Note *n2 = nil;
//
//    for (Element *element in _elements) {
//        Note *note = nil;
//        if ([element isKindOfClass:[Note class]]) {
//            note = (Note *)element;
//        } else {
//            continue;
//        }
//        
//        [note setMirror:mirrored];
//        
//        float newHeight = ((note.x - xOffset) * slope) +
//        (abs(pivot.line - note.line) * (config.lines.spacing / 2.0)) + pivotHeight;
//        
//        [note setStemHeight:newHeight];
//        [note updateStemPoints];
//    }
//    
//    n1 = _elements[0];
//    n2 = _elements[[_elements count] - 1];
//    
//    Beam *beam = [[Beam alloc] initWithLength:n2.x-n1.x slope:slope];
//    
//    [n1 setBeam:beam];
//    
//}

- (void)draw: (CGContextRef)ctx {
    assert([_tickables count] > 1);
    
//    Element *first = _elements[0];
//    Element *last = _elements[[_elements count] -1];
//    
//    float yf = first.line * ([MusicNotationConfig sharedInstance].lines.spacing / 2.0);
//    float yl = last.line * ([MusicNotationConfig sharedInstance].lines.spacing / 2.0);
//    [self drawBeam:ctx
//        startPoint:CGPointMake(first.x + first.stemOrigin.x,
//                               yf + first.stemHeight + first.stemOrigin.y)
//          endPoint:CGPointMake(last.x + last.stemOrigin.x,
//                               yl + last.stemHeight + last.stemOrigin.y)];
}

#pragma mark - Private Methods

- (StemDirection)calculateStemDirection {
    int sum = 0;
    
    for (Tickable *t in _tickables) {
        sum += [t line];
    }
    
    if (sum >= 0) {
        return StemDownwards;
    } else {
        return StemUpwards;
    }
}

- (Tickable *)topTickable {
    assert([_tickables count] >= 2);
    
    int topLine = [(Tickable *)_tickables[0] line];
    Tickable *topTickable = _tickables[0];
    
    int count = [_tickables count];
    for (int i = 1; i < count; i++) {
        int line = [(Tickable *)_tickables[i] line];
        
        if (line > topLine) {
            topLine = line;
            topTickable = _tickables[i];
        }
    }
    
    return topTickable;
}

- (Tickable *)bottomTickable {
    assert([_tickables count] >= 2);
    
    int bottomLine = [(Tickable *)_tickables[0] line];
    Tickable *bottomTickable = _tickables[0];
    
    int count = [_tickables count];
    for (int i = 1; i < count; i++) {
        int line = [(Tickable *)_tickables[i] line];
        
        if (line < bottomLine) {
            bottomLine = line;
            bottomTickable = _tickables[i];
        }
    }
    
    return bottomTickable;
}

- (Tickable *)pivotTickable {
    int count = [_tickables count];
    
    assert(count >= 2);
    
    Tickable *pivot = _tickables[0];
    float mEndPoint = 0.0;
    
    if (_stemDirection == StemUpwards) {
        mEndPoint = [pivot measureStemOriginY] + [pivot minHeight];
    } else {
        mEndPoint = [pivot measureStemOriginY] - [pivot minHeight];
    }
    
    for (int i = 1; i < count; i++) {
        Tickable *t = (Tickable *)_tickables[i];
        float tmp = 0.0;
        
        if (_stemDirection == StemUpwards) {
            tmp = [t measureStemOriginY] + [t minHeight];
            
            if (tmp > mEndPoint) {
                mEndPoint = tmp;
                pivot = t;
            }
        } else {
            tmp = [t measureStemOriginY] - [t minHeight];
            
            if (tmp < mEndPoint) {
                mEndPoint = tmp;
                pivot = t;
            }
        }
    }
    
    return pivot;
}

- (float)calculateSlope {
    int count = [_tickables count];
    
    assert(count >= 2);
    
    Tickable *first = _tickables[0];
    Tickable *last = _tickables[count - 1];
    
    if (first.line == last.line) {
        return 0.0;
    }
    
    float halfLineSpace = MNCONFIG.lines.spacing / 2.0;
    float maxSlope = MNCONFIG.beams.maxSlope;
    float slope = ((last.line - first.line) * halfLineSpace) / (last.x - first.x);
    
    if (fabsf(slope) > maxSlope) {
        slope = maxSlope * (slope < 0.0 ? -1.0 : 1.0);
    }
    
    printf("> Slope: %.2f\n", slope);
    return slope;
}

- (int)minNumOfFlags {
    int min = [_tickables[0] numOfFlags];
    
    for (Tickable *t in _tickables) {
        if ([t numOfFlags] < min) {
            min = [t numOfFlags];
        }
    }
    
    return min;
}

- (int)maxNumOfFlags {
    int max = [_tickables[0] numOfFlags];
    
    for (Tickable *t in _tickables) {
        if ([t numOfFlags] > max) {
            max = [t numOfFlags];
        }
    }
    
    return max;
}

- (void)drawBeam: (CGContextRef)ctx startPoint:(CGPoint)sp endPoint:(CGPoint)ep {
    CGContextMoveToPoint(ctx, sp.x, sp.y);
    CGContextAddLineToPoint(ctx, ep.x, ep.y);
    CGContextSetLineWidth(ctx, 2.0);
    CGContextStrokePath(ctx);
}

@end
