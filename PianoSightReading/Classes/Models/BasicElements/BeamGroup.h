//
//  Beam.h
//  PianoSightReading
//
//  Created by Tiago Bras on 23/12/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Tickable.h"

@interface BeamGroup : NSObject

@property(nonatomic, strong) NSMutableArray *tickables;
@property(nonatomic, assign) int quarterNoteTick;
@property(nonatomic, assign) StemDirection stemDirection;
@property(nonatomic, assign) BOOL stemDirectionLocked;


- (id)initWithQuarterNoteTick: (int)quarterNoteTick;
- (id)initWithTickables: (NSArray *)tickables quarterNoteTick: (int)quarterNoteTick;

- (void)addTickable: (Element *)element;
- (void)layout;
- (void)draw: (CGContextRef)ctx;


@end
