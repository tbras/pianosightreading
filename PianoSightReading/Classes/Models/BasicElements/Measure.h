//
//  Measure.h
//  PianoSightReading
//
//  Created by Tiago Bras on 25/10/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TheoryHelper.h"

@class Voice;

@interface Measure : NSObject

@property(nonatomic, strong) NSMutableArray *voices;

@property(nonatomic, assign) Clef clef;
@property(nonatomic, assign) Key key;
@property(nonatomic, assign) TimeSignature timeSignature;

// Graphic variables
@property(nonatomic, assign) BOOL drawClef;
@property(nonatomic, assign) BOOL drawKeySignature;
@property(nonatomic, assign) BOOL drawTimeSignature;
@property(nonatomic, strong) NSArray *beamGroups;

- (id)initWithVoices: (NSArray *)voices
                clef: (Clef)clef
                 key: (Key)key
       timeSignature: (TimeSignature)timeSignature;

- (id)initWithClef: (Clef)clef
               Key: (Key)key
     timeSignature: (TimeSignature)timeSignature;

- (int)countVoices;
- (void)addVoice: (Voice *)voice;

- (BOOL)validateVoices;
- (float)totalMinWidth;

- (NSArray *)alignedAccTickablesIndexes:(NSArray *)acc1 acc2:(NSArray *)acc2;
- (NSArray *)alignedTickablesIndexes: (Voice *)v1 v2:(Voice *)v2;

- (float)layout: (float)justifiedWidth;
- (void)draw: (CGContextRef)ctx;

@end

// Helper class to store aligned elements at what accumulated tick
@interface AlignedTickable : NSObject

@property(nonatomic, assign) int tick;
@property(nonatomic, assign) int indexA;
@property(nonatomic, assign) int indexB;

- (id)initWithTick: (int)tick indexA:(int)indexA indexB:(int)indexB;

@end

