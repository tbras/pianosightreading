//
//  Staff.m
//  PianoSightReading
//
//  Created by Tiago Bras on 25/10/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import "Staff.h"
#import "TheoryHelper.h"
#import "MusicNotationConfig.h"

@implementation Staff

@synthesize measures = _measures;
@synthesize clef = _clef;

@synthesize frame = _frame;
@synthesize middleLineY = _middleLineY;

- (id)initWithClef: (Clef)clef measures: (NSArray *)measures frame:(CGRect)frame {
    self = [super init];
    if (self) {
        self.clef = clef;
        self.measures = measures;
        self.frame = frame;
        self.middleLineY = frame.origin.y + floorf(frame.size.height / 2.0);
    }
    return self;
}

- (float)layout: (float)justifiedWidth xOffset:(float)xOffset {
    
}

- (void)draw: (CGContextRef)ctx {
    // Draw staff
    CGContextSetShouldAntialias(ctx, FALSE);
    
    float space = [MusicNotationConfig sharedInstance].lines.spacing;
    
    CGContextSaveGState(ctx);
    
    CGContextTranslateCTM(ctx, _frame.origin.x, _middleLineY - (space * 2.0));
    
    // First bar line
    CGContextSetLineWidth(ctx, 2.0);
    
    CGContextMoveToPoint(ctx, 0.0, 0.0);
    CGContextAddLineToPoint(ctx, 0.0, space * 4.0);
    CGContextStrokePath(ctx);
    
    // Draw last bar
    CGContextMoveToPoint(ctx, _frame.size.width, 0.0);
    CGContextAddLineToPoint(ctx, _frame.size.width, space * 4.0);
    CGContextStrokePath(ctx);
    
    printf("Spacing: %.2f\n", space);
    
    //    CGContextSetLineWidth(ctx, _config.lines.thickness);
    CGContextSetLineWidth(ctx, 1.0);
    for (int i = 0; i < 5; i++) {
        // Horizontal measure line
        CGContextMoveToPoint(ctx, 0.0, 0.0);
        CGContextAddLineToPoint(ctx, _frame.size.width, 0.0);
        CGContextStrokePath(ctx);
        
        CGContextTranslateCTM(ctx, 0.0, space);
    }
    
    CGContextRestoreGState(ctx);
    
    if ([_measures count] <= 0) {
        return;
    }
    
    // Draw measures
    CGContextSaveGState(ctx);
    
    CGContextTranslateCTM(ctx, 0.0, _middleLineY);
    
    int size = [_measures count];
    float xOffset = _frame.origin.x;
    
    Measure *m = [_measures objectAtIndex:0];
    
    [m setDrawClef:YES];
    [m setDrawKeySignature:YES];
//    [m setDrawTimeSignature:YES];
    for (Measure *m in _measures) {
        
        CGContextTranslateCTM(ctx, xOffset, 0.0);
        xOffset = [m layout:(_frame.size.width / (float)size)];
        [m draw:ctx];
        
        CGContextSaveGState(ctx);
        //  bar line
        CGContextSetLineWidth(ctx, 1.0);
        
        CGContextMoveToPoint(ctx, xOffset, space * -2.0);
        CGContextAddLineToPoint(ctx, xOffset, space * 2.0);
        CGContextStrokePath(ctx);
        CGContextRestoreGState(ctx);
    }
    
    CGContextRestoreGState(ctx);

}

@end
