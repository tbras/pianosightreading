//
//  Staff.h
//  PianoSightReading
//
//  Created by Tiago Bras on 25/10/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Measure.h"


@interface Staff : NSObject

@property(nonatomic, strong) NSArray *measures;
@property(nonatomic, assign) Clef clef;

// Graphics
@property(nonatomic, assign) CGRect frame;
@property(nonatomic, assign) float middleLineY;

- (id)initWithClef: (Clef)clef measures: (NSArray *)measures frame:(CGRect)frame;

- (float)layout: (float)justifiedWidth xOffset:(float)xOffset;
- (void)draw: (CGContextRef)ctx;
@end
