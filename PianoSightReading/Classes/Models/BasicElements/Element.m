//
//  MeasureElement.m
//  PianoSightReading
//
//  Created by Tiago Bras on 26/10/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import "Element.h"
#import "Rest.h"
#import "Note.h"

@implementation Element

@synthesize minWidth = _minWidth;
@synthesize width = _width;
@synthesize x = _x;
@synthesize hidden = _hidden;
@synthesize leftElement = _leftElement;

- (id)init {
    if ((self = [super init])) {
        _minWidth = 0.0;
        _width = 0.0;
        _x = 0.0;
        _hidden = FALSE;
        _leftElement = nil;
    }
    
    return self;
}

#pragma mark -
#pragma Public Methods


- (void)draw: (CGContextRef)ctx {
    // Nothing... override it
}


@end
