//
//  Note.h
//  PianoSightReading
//
//  Created by Tiago Bras on 25/10/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Tickable.h"
#import "Measure.h"
#import "Element.h"
#import "NotationSymbols.h"
#import "TheoryHelper.h"

typedef enum NoteHead {
    NoteHeadWhole = Sym_noteheads_whole,
    NoteHeadHalf = Sym_noteheads_half,
    NoteHeadQuarter = Sym_noteheads_quarter
}NoteHead;

typedef enum Articulation {
    ArticulationNone = 0,
    ArticulationStaccato,
    ArticulationFermataTop,
    ArticulationFermataBottom
}Articulation;

typedef enum Accidental {
    AccidentalNone = -3,
    AccidentalFlatFlat = -2,
    AccidentalFlat = -1,
    AccidentalNatural = 0,
    AccidentalSharp = 1,
    AccidentalSharpSharp = 2
}Accidental;


@interface Note : Tickable

// Sound related
@property(nonatomic, assign) MidiPitch pitch;
@property(nonatomic, assign) int velocity;
@property(nonatomic, assign) int pitchOffset;

// Graphics related
@property(nonatomic, assign) NoteHead noteHead;
@property(nonatomic, assign) Accidental accidental;
@property(nonatomic, assign) Clef clef;
@property(nonatomic, assign) int numOfDots; // A dot means "add half the value of the note"
@property(nonatomic, assign) BOOL drawLedgersLines;

@property(nonatomic, assign) Articulation articulation;

- (id)initWithPitch: (MidiPitch)pitch tick: (int)tick;
- (id)initWithPitch: (MidiPitch)pitch tick: (int)tick velocity:(int)velocity;

- (void)update: (Key)key;
- (void)draw: (CGContextRef)ctx;

@end
