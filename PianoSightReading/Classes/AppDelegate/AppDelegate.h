//
//  AppDelegate.h
//  PianoSightReading
//
//  Created by Tiago Bras on 18/10/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UISplitViewController *splitViewController;

@end
