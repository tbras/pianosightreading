//
//  TBUtils.h
//  PianoSightReading
//
//  Created by Tiago Bras on 26/10/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <stdlib.h>

#define printPoint(p) printf("Point: (%.2f, %.2f)\n", p.x, p.y)
#define printSize(s) printf("Size: (%.2f, %.2f)\n", s.width, s.height)
#define printRect(r) printf("Rect: (%.2f, %.2f) - (%.2f, %.2f)\n", \
r.origin.x, r.origin.y, r.size.width, r.size.height)
#define printFrame(f) printRect(f)

typedef struct IntegerCArray{
    int size;
    int *array;
}IntegerCArray;

IntegerCArray IntegerCArrayAlloc(u_int size);
void IntegerCArrayFree(IntegerCArray integerCArray);
IntegerCArray NumberArrayToIntegerCArray(NSArray *array);

@interface TBUtils : NSObject

+ (int *)numberArrayToCArray: (NSArray *)array;
+ (BOOL)containsInteger: (NSArray *)array integer:(int)integer;

@end
