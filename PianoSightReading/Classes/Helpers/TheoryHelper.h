//
//  TheoryHelper.h
//  PianoSightReading
//
//  Created by Tiago Bras on 01/12/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import <Foundation/Foundation.h>

// !!! WARNING: Dont change the enumerations !!!

//typedef enum NoteType {
//    WholeNote = 0,
//    HalfNote,
//    QuarterNote,
//    EigthNote,
//    SixteenthNote,
//    ThirtySecondNote,
//    SixtyFourthNote
//}NoteType;

// List of all used keys (non-sensed keys excluded)
typedef enum Key {
    // Major Keys
    KeyMajorCb = 0, KeyMajorC, KeyMajorCSharp,
    KeyMajorDb, KeyMajorD,
    KeyMajorEb, KeyMajorE,
    KeyMajorF, KeyMajorFSharp,
    KeyMajorGb, KeyMajorG,
    KeyMajorAb, KeyMajorA,
    KeyMajorBb, KeyMajorB,
    
    // Minor Keys
    KeyMinorC, KeyMinorCSharp,
    KeyMinorD, KeyMinorDSharp,
    KeyMinorEb, KeyMinorE,
    KeyMinorF, KeyMinorFSharp,
    KeyMinorG, KeyMinorGSharp,
    KeyMinorAb, KeyMinorA, KeyMinorASharp,
    KeyMinorBb, KeyMinorB,
    
    KeysCount
}Key;

typedef enum MidiPitch {
    MidiPitchA0 = 21,
    MidiPitchA0Sharp = 22,
    MidiPitchB0 = 23,
    MidiPitchC1 = 24,
    MidiPitchC1Sharp = 25,
    MidiPitchD1 = 26,
    MidiPitchD1Sharp = 27,
    MidiPitchE1 = 28,
    MidiPitchF1 = 29,
    MidiPitchF1Sharp = 30,
    MidiPitchG1 = 31,
    MidiPitchG1Sharp = 32,
    MidiPitchA1 = 33,
    MidiPitchA1Sharp = 34,
    MidiPitchB1 = 35,
    MidiPitchC2 = 36,
    MidiPitchC2Sharp = 37,
    MidiPitchD2 = 38,
    MidiPitchD2Sharp = 39,
    MidiPitchE2 = 40,
    MidiPitchF2 = 41,
    MidiPitchF2Sharp = 42,
    MidiPitchG2 = 43,
    MidiPitchG2Sharp = 44,
    MidiPitchA2 = 45,
    MidiPitchA2Sharp = 46,
    MidiPitchB2 = 47,
    MidiPitchC3 = 48,
    MidiPitchC3Sharp = 49,
    MidiPitchD3 = 50,
    MidiPitchD3Sharp = 51,
    MidiPitchE3 = 52,
    MidiPitchF3 = 53,
    MidiPitchF3Sharp = 54,
    MidiPitchG3 = 55,
    MidiPitchG3Sharp = 56,
    MidiPitchA3 = 57,
    MidiPitchA3Sharp = 58,
    MidiPitchB3 = 59,
    MidiPitchC4 = 60,
    MidiPitchC4Sharp = 61,
    MidiPitchD4 = 62,
    MidiPitchD4Sharp = 63,
    MidiPitchE4 = 64,
    MidiPitchF4 = 65,
    MidiPitchF4Sharp = 66,
    MidiPitchG4 = 67,
    MidiPitchG4Sharp = 68,
    MidiPitchA4 = 69,
    MidiPitchA4Sharp = 70,
    MidiPitchB4 = 71,
    MidiPitchC5 = 72,
    MidiPitchC5Sharp = 73,
    MidiPitchD5 = 74,
    MidiPitchD5Sharp = 75,
    MidiPitchE5 = 76,
    MidiPitchF5 = 77,
    MidiPitchF5Sharp = 78,
    MidiPitchG5 = 79,
    MidiPitchG5Sharp = 80,
    MidiPitchA5 = 81,
    MidiPitchA5Sharp = 82,
    MidiPitchB5 = 83,
    MidiPitchC6 = 84,
    MidiPitchC6Sharp = 85,
    MidiPitchD6 = 86,
    MidiPitchD6Sharp = 87,
    MidiPitchE6 = 88,
    MidiPitchF6 = 89,
    MidiPitchF6Sharp = 90,
    MidiPitchG6 = 91,
    MidiPitchG6Sharp = 92,
    MidiPitchA6 = 93,
    MidiPitchA6Sharp = 94,
    MidiPitchB6 = 95,
    MidiPitchC7 = 96,
    MidiPitchC7Sharp = 97,
    MidiPitchD7 = 98,
    MidiPitchD7Sharp = 99,
    MidiPitchE7 = 100,
    MidiPitchF7 = 101,
    MidiPitchF7Sharp = 102,
    MidiPitchG7 = 103,
    MidiPitchG7Sharp = 104,
    MidiPitchA7 = 105,
    MidiPitchA7Sharp = 106,
    MidiPitchB7 = 107,
    MidiPitchC8 = 108,
    
    MidiMiddleC = MidiPitchC4
}MidiPitch;

// Don't change this enum. Each value represents the semitone degree
typedef enum ScaleDegree {
    SD_I = 0,
    SD_IIb = 1,
    SD_II = 2,
    SD_IIIb = 3,
    SD_III = 4,
    SD_IVb = 4,
    SD_IV = 5,
    SD_Vb = 6,
    SD_V = 7,
    SD_VIb = 8,
    SD_VI = 9,
    SD_VIIb = 10,
    SD_VII = 11,
    
    SD_VIII = SD_I,
    SD_IX = SD_II,
    SD_X = SD_III,
    SD_XI = SD_IV,
    SD_XII = SD_V,
    SD_XIII = SD_IV
}ScaleDegree;

typedef enum Clef {
    TrebleClef = 0,
    BassClef,
    AltoClef
}Clef;

typedef struct TimeSignature {
    int quantity;
    int quality;
}TimeSignature;

CG_INLINE TimeSignature
TimeSignatureMake(int quantity, int quality)
{
    TimeSignature ts; ts.quantity = quantity; ts.quality = quality; return ts;
}

#define TimeSignature4_4 TimeSignatureMake(4, 4)
#define TimeSignature3_4 TimeSignatureMake(3, 4)
#define TimeSignature2_4 TimeSignatureMake(2, 4)

@interface TheoryHelper : NSObject

// Positive numbers are sharps, negative numbers are flats
+ (int)signatureForKey: (Key)key;

// Gets the semitone degree in relation to C Major, for example:
// KeyMajorC = 0
// KeyMajorCB = -1
// KeyMajorE4
// KeyMajorB = 11
+ (int)semitoneInCMajor: (Key)key;

// Gets de semitone degree for a pitch in a key
// Example: MidiPitchD4 is the 2 semitone degree in CMajor, but it's the 0 in DMajor
// In a Major scale the convertion from semitone degree to scale degree is:
// I = 0, II = 2, III = 4, IV = 5, V = 7, VI = 9, VII = 11, VIII = 0 (and 12?)
+ (int)semitoneDegreeForPitch: (MidiPitch)pitch key: (Key)key;

// Converts scale degree in semitone degree
+ (int)semitoneDegreeForScaleDegree: (ScaleDegree)scaleDegree;

+ (int)octaveForPitch: (MidiPitch)pitch key:(Key)key;

+ (int)lineForPitch: (MidiPitch)pitch key:(Key)key clef:(Clef)clef;

+ (GlyphCode)clefGlyph: (Clef)clef;

@end
