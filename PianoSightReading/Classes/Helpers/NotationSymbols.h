//
//  NotationSymbols.h
//  PianoSightReading
//
//  Created by Tiago Bras on 28/11/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreText/CoreText.h>

// Note heads widths fo Gonville-20 at 30.
#define QUARTER_NOTE_WIDTH (9.0)
#define STAFF_LINE_SPACE (9)
#define STAFF_LINE_SPACE_HALF (4.5)

typedef enum NotationFont {
    FontGonville = 0
}NotationFont;

typedef enum GlyphCode {
    Sym_0 = 0,
    Sym_1,
    Sym_2,
    Sym_3,
    Sym_4,
    Sym_5,
    Sym_6,
    Sym_7,
    Sym_8,
    Sym_9,
    Sym_f,
    Sym_m,
    Sym_p,
    Sym_s,
    Sym_r,
    Sym_z,
    Sym_dot,
    Sym_clefs_g,
    Sym_clefs_change_g,
    Sym_clefs_f,
    Sym_clefs_change_f,
    Sym_clefs_c,
    Sym_clefs_change_c,
    Sym_coda,
    Sym_varcoda,
    Sym_ufermata,
    Sym_dfermata,
    Sym_accidentals_flat,
    Sym_accidentals_flatflat,
    Sym_accidentals_sharp,
    Sym_accidentals_sharpsharp,
    Sym_accidentals_natural,
    Sym_noteheads_whole,
    Sym_noteheads_half,
    Sym_noteheads_quarter,
    Sym_flags_d8,
    Sym_flags_d16,
    Sym_flags_d32,
    Sym_flags_d64,
    Sym_flags_u8,
    Sym_flags_u16,
    Sym_flags_u32,
    Sym_flags_u64,
    Sym_rests_whole,
    Sym_rests_half,
    Sym_rests_quarter,
    Sym_rests_eighth,
    Sym_rests_sixteenth,
    Sym_rests_thirtysecond,
    Sym_rests_sixtyfourth,
    Sym_scripts_staccato,
    Sym_space,
} GlyphCode;

@interface NotationSymbols : NSObject {
    NSDictionary *gonvilleMap;
    
    CTFontRef gonvilleFont;
}

+ (NotationSymbols *)sharedInstance;
- (NSString *)stringForSymbol: (GlyphCode)code inFont:(NotationFont)font;
- (UIBezierPath *)bezierPathFromGlyph:(GlyphCode)code;
- (void)drawGlyph:(GlyphCode)code;



@end
