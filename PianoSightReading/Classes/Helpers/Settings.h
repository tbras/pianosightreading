//
//  Settings.h
//  PianoSightReading
//
//  Created by Tiago Bras on 28/11/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Settings : NSObject 

+ (Settings *)sharedInstance;

@end
