//
//  Settings.m
//  PianoSightReading
//
//  Created by Tiago Bras on 28/11/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import "Settings.h"

@implementation Settings

+ (Settings *)sharedInstance {
    //  Static local predicate must be initialized to 0
    static Settings *sharedInstance = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[Settings alloc] init];
    });
    return sharedInstance;
}

@end
