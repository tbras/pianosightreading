//
//  NotationSymbols.m
//  PianoSightReading
//
//  Created by Tiago Bras on 28/11/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import "NotationSymbols.h"

@interface NotationSymbols()
@property(nonatomic, strong) NSDictionary *gonvilleMap;
@property(nonatomic, assign) CTFontRef gonvilleFont;
@end

@implementation NotationSymbols

@synthesize gonvilleMap = _gonvilleMap;
@synthesize gonvilleFont = _gonvilleFont;

// TODO: Implementar cache

+ (NotationSymbols *)sharedInstance {
    //  Static local predicate must be initialized to 0
    static NotationSymbols *sharedInstance = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[NotationSymbols alloc] init];
        
        int size = (int)((STAFF_LINE_SPACE * 20.0) / 5.0);
        
        printf("Font size: %d\n", size);
        
        CTFontRef gonvilleFont = CTFontCreateWithName(CFSTR("Gonville-20"), size, NULL);
        
        if (gonvilleFont == nil) {
            NSLog(@"Gonville font not loaded/found.");
        }
        
        [sharedInstance setGonvilleFont:gonvilleFont];
        
        NSDictionary *gmap = @{
        @(Sym_0) : @"0",
        @(Sym_1) : @"1",
        @(Sym_2) : @"2",
        @(Sym_3) : @"3",
        @(Sym_4) : @"4",
        @(Sym_5) : @"5",
        @(Sym_6) : @"6",
        @(Sym_7) : @"7",
        @(Sym_8) : @"8",
        @(Sym_9) : @"9",
        @(Sym_f) : @"f",
        @(Sym_m) : @"m",
        @(Sym_p) : @"p",
        @(Sym_s) : @"s",
        @(Sym_r) : @"r",
        @(Sym_z) : @"z",
        @(Sym_dot) : @"\ue17c",
        @(Sym_clefs_g) : @"\ue118",
        @(Sym_clefs_change_g) : @"\ue11d",
        @(Sym_clefs_f) : @"\ue117",
        @(Sym_clefs_change_f) : @"\ue11c",
        @(Sym_clefs_c) : @"\ue116",
        @(Sym_clefs_change_c) : @"\ue11b",
        @(Sym_coda) : @"\ue120",
        @(Sym_varcoda) : @"\ue121",
        @(Sym_ufermata) : @"\ue122",
        @(Sym_dfermata) : @"\ue126",
        @(Sym_accidentals_flat) : @"\ue12e",
        @(Sym_accidentals_flatflat) : @"\ue137",
        @(Sym_accidentals_sharp) : @"\ue170",
        @(Sym_accidentals_sharpsharp) : @"\ue178",
        @(Sym_accidentals_natural) : @"\ue14f",
        @(Sym_noteheads_whole) : @"\ue169",
        @(Sym_noteheads_half) : @"\ue13f",
        @(Sym_noteheads_quarter) : @"\ue13e",
        @(Sym_flags_d8) : @"\ue17f",
        @(Sym_flags_d16) : @"\ue181",
        @(Sym_flags_d32) : @"\ue183",
        @(Sym_flags_d64) : @"\ue185",
        @(Sym_flags_u8) : @"\ue180",
        @(Sym_flags_u16) : @"\ue182",
        @(Sym_flags_u32) : @"\ue184",
        @(Sym_flags_u64) : @"\ue186",
        @(Sym_rests_whole) : @"\ue162",
        @(Sym_rests_half) : @"\ue166",
        @(Sym_rests_quarter) : @"\ue15d",
        @(Sym_rests_eighth) : @"\ue164",
        @(Sym_rests_sixteenth) : @"\ue165",
        @(Sym_rests_thirtysecond) : @"\ue15f",
        @(Sym_rests_sixtyfourth) : @"\ue160",
        @(Sym_scripts_staccato) : @"\ue17b",
        @(Sym_space): @" "};
        
        [sharedInstance setGonvilleMap:gmap];
    });
    return sharedInstance;
}

- (NSString *)stringForSymbol: (GlyphCode)code inFont:(NotationFont)font {
    if (font == FontGonville) {
        return _gonvilleMap[@(code)];
    } else {
        printf("Font with id = %d not found. Using Gonville instead.\n", font);
        
        return _gonvilleMap[@(code)];
    }
}

- (UIBezierPath *)bezierPathFromGlyph:(GlyphCode)code {
    // Buffers
    unichar chars[1];
    CGGlyph glyphs[1];
    
    // Copy the character into a buffer
    chars[0] = [_gonvilleMap[@(code)] characterAtIndex:0];
    
    // Encode the glyph for the single character into another buffer
    CTFontGetGlyphsForCharacters(_gonvilleFont, chars, glyphs, 1);
    
    // Get the single glyph
    CGGlyph aGlyph = glyphs[0];
    
    // Find a reference to the Core Graphics path for the glyph
    CGPathRef glyphPath = CTFontCreatePathForGlyph(_gonvilleFont, aGlyph, NULL);
    
    // Create a bezier path from the CG path
    UIBezierPath *glyphBezierPath = [UIBezierPath bezierPath];
    [glyphBezierPath moveToPoint:CGPointZero];
    [glyphBezierPath appendPath:[UIBezierPath bezierPathWithCGPath:glyphPath]];
    
    CGPathRelease(glyphPath);
    
    return glyphBezierPath;
}

- (void)drawGlyph:(GlyphCode)code {
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextSaveGState(ctx);
    {
        CGContextScaleCTM(ctx, 1.0, -1.0);
    
        [[self bezierPathFromGlyph:code] fill];
    }
    CGContextRestoreGState(ctx);
}

@end
