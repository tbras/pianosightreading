//
//  TBUtils.m
//  PianoSightReading
//
//  Created by Tiago Bras on 26/10/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import "TBUtils.h"

IntegerCArray IntegerCArrayAlloc(u_int size) {
    IntegerCArray a;
    a.size = size;
    a.array = malloc(sizeof(int) * size);
    
    if (a.array == NULL) {
        fprintf(stderr, "THE IMPOSSIBLE HAPPENED!!! Not enough memory.\n");
        exit(EXIT_FAILURE);
    }
    
    return a;
}

void IntegerCArrayFree(IntegerCArray integerCArray) {
    if (integerCArray.array != NULL) {
        free(integerCArray.array);
        integerCArray.array = NULL;
    }
}

IntegerCArray NumberArrayToIntegerCArray(NSArray *array) {
    int size = [array count];
    
    IntegerCArray intArray = IntegerCArrayAlloc(size);
    
    for (int i = 0; i < size; i++) {
        intArray.array[i] = [[array objectAtIndex:i] intValue];
    }
    
    return intArray;
}

@implementation TBUtils

+ (int *)numberArrayToCArray: (NSArray *)array {
    int size = [array count];
    
    if (size <= 0) {
        return NULL;
    }
    
    int *cArray = malloc(size * sizeof(int));
    
    if (cArray == NULL) {
        fprintf(stderr, "Failed to alloc memory to cArray\n.");
        return NULL;
    }
    
    for (int i = 0; i < size; i++) {
        cArray[i] = [[array objectAtIndex:i] intValue];
    }
    
    return cArray;
}

@end
