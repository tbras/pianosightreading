//
//  TheoryHelper.m
//  PianoSightReading
//
//  Created by Tiago Bras on 01/12/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import "TheoryHelper.h"

@implementation TheoryHelper

static int signatureForKeyMap[KeysCount] = {-7, 0, 7, -5, 2, -3, 4, -1, 6, -6, 1, -4, 3, -2, 5, -3, 4, -1, 6, -6, 1, -4, 3, -2, 5, -7, 0, 7, -5, 2};

+ (int)signatureForKey:(Key)key {
    assert((int)key >= 0 && (int)key < KeysCount);
    
    return signatureForKeyMap[key];
}

// TODO: substituir por um array ke mapea os valores
+ (int)semitoneInCMajor: (Key)key {
    switch (key) {
        case KeyMajorCb: return -1;
        case KeyMajorC: return 0;
        case KeyMinorC: return 0;
        case KeyMajorCSharp: return 1;
        case KeyMinorCSharp: return 1;
            
        case KeyMajorDb: return 1;
        case KeyMajorD: return 2;
        case KeyMinorD: return 2;
        case KeyMinorDSharp: return 3;
            
        case KeyMajorEb: return 3;
        case KeyMinorEb: return 3;
        case KeyMajorE: return 4;
        case KeyMinorE: return 4;
            
        case KeyMajorF: return 5;
        case KeyMinorF: return 5;
        case KeyMajorFSharp: return 6;
        case KeyMinorFSharp: return 6;
            
        case KeyMajorGb: return 6;
        case KeyMajorG: return 7;
        case KeyMinorG: return 7;
        case KeyMinorGSharp: return 8;
            
        case KeyMajorAb: return 8;
        case KeyMinorAb: return 8;
        case KeyMajorA: return 9;
        case KeyMinorA: return 9;
        case KeyMinorASharp: return 10;
            
        case KeyMajorBb: return 10;
        case KeyMinorBb: return 10;
        case KeyMajorB: return 11;
        case KeyMinorB: return 11;
        default:
            return 0;
    }
}

+ (int)semitoneDegreeForPitch: (MidiPitch)pitch key: (Key)key {
    int keyDegree = [self semitoneInCMajor:key];
    
    return (pitch - keyDegree) % 12;
}

+ (int)semitoneDegreeForScaleDegree: (ScaleDegree)scaleDegree {
    return (int)scaleDegree;
}

+ (int)octaveForPitch: (MidiPitch)pitch key:(Key)key {
    int keyDegree = [self semitoneInCMajor:key];
    
    return ((pitch) / 12) - 1;
}

+ (int)lineForPitch: (MidiPitch)pitch key:(Key)key clef:(Clef)clef {
    int octave = [self octaveForPitch:pitch key:key];
    int degree = [self semitoneDegreeForPitch:pitch key:key];
    int offset = [self semitoneInCMajor:key];
    
    if (offset < 0) {
        octave += -offset;
        offset = 0;
    }
    
    if (degree + offset > 11) {
        degree = (degree + offset) - 12;
        offset = 0;
    }
    
    if (clef == TrebleClef) {
        // Line in octave 5
        int keyDegreeToLineMap[12] = {1, 1, 2, 2, 3, 4, 4, 5, 5, 6, 6, 7};
        
        int preLine = keyDegreeToLineMap[degree + offset];
        
        int line = preLine + (7 * (octave - 5));
        
        return line;
    } else if (clef == BassClef) {
        // Line in octave 3
        int keyDegreeToLineMap[12] = {-1, -1, 0, 0, 1, 2, 2, 3, 3, 4, 4, 5};
        
        int preLine = keyDegreeToLineMap[degree + offset];
        
        return preLine + (7 * (octave - 3));
    } else {
        // TODO: implement for other clefs like alto clef
        assert(TRUE);
    }
    
    return 0;
}

+ (GlyphCode)clefGlyph: (Clef)clef {
    if (clef == TrebleClef) {
        return Sym_clefs_g;
    } else if (clef == BassClef) {
        return Sym_clefs_f;
    } else if (clef == AltoClef) {
        return Sym_clefs_c;
    } else {
        return Sym_clefs_g;
    }
}

@end
