//
//  MasterViewController.h
//  PianoSightReading
//
//  Created by Tiago Bras on 18/10/12.
//  Copyright (c) 2012 Tiago Bras. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;

@end
